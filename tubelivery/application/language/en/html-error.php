<?php

$home_page = HTML::link('/', 'home page');

return array(
	/**
	 * English
	 */
	'lost' => array(
		'We need a map.',
		"I think we're lost.",
		'We took a wrong turn.'
	),
	'problem' => array(
		'Ouch.',
		'Oh no!',
		'Whoops!'
	),
	'meaning_title' => 'What does this mean?',
	'redirect' => "Perhaps you would like to go to our $home_page?",
	'error_404' => "Server Error: 404 (Not Found)",
	'error_500' => "Server Error: 500 (Internal Server Error)",
	'meaning_text_404' => "We couldn't find the page you requested on our
		servers. We're really sorry about that.",
	'meaning_text_500' => "Something went wrong on our servers while we were
		processing your request. We're really sorry about this, and will work
		hard to get this resolved as soon as possible."
);
