<?php

/**
 * English
 */
return array(
	'lang' => 'en',
	'other_lang' => 'pt',
	'locale' => 'en_US',
	'tubelivery_slogan' => 'Create podcasts from YouTube channels',
	'login_error' => 'Username or password incorrect.',
	'welcome_back' => 'Welcome back, :name! Did you know you can add as many channels you want with your plan?',
	'menu_plans' => 'Plans',
	'menu_login' => 'log in',
	'menu_signup' => 'sign up',
	'menu_channels' => 'My channels',
	'menu_account' => 'My account',
	'menu_support' => 'support',
	'menu_logout' => 'log out',
	'name' => 'Nickname',
	'username' => 'Username',
	'email' => 'Email',
	'password' => 'Password',
	'remember_me' => 'Remember me',
	'login' => 'Log in with',
	'login_facebook' => 'Facebook',
	'signup' => 'Sign up with',
	'signup_facebook' => 'Facebook',
	'or_email' => 'or email',
	'logout' => 'Log ut',
	'channel' => 'Channel',
	'channels' => 'Channels',
	'account' => 'Account',
	'add_channel' => 'Add channel',
	'channel_name' => 'Channel name or URL',
	'videos' => 'videos',
	'subscribers' => 'subscribers',
	'exibitions' => 'exibitions',
	'view_on_yt' => 'View on YouTube',
	'open_podcast' => 'Open Podcast',
	'open_in_itunes' => 'Open in iTunes',
	'delete_podcast' => 'Delete podcast',
	'podcast_deletion' => 'Podcast exclusion',
	'podcast_deletion_text' => 'Are you shure you want to exclude the podcast
		from YouTube channel ":label"? Depending on your plan, this action can\'t
		be undone!',
	'cancel' => 'Cancel',
	'quality' => 'Quality',
	'footer_about' => 'About',
	'footer_version' => 'Change log',
	'footer_terms' => 'Terms of use',
	'copyright' => '© ' . Config::get('application.copyright_year') .
	' Tubelivery',
	'youtube_unreachable' => "We coudn't connct to YouTube. Please, try again
		latter.",
	'youtube_channel_not_found' => 'We coudn\'t find the channel "
		:yt_username".',
	'youtube_channel_added' => 'Channel ":channel_label" added.',
	'youtube_channel_already_in_user' => 'You had already signed ":channel_label"
		channel.',
//	'plan_channel_limit_reached' => 'The limit of channels (:channels) for the
//		:plan_label plan has been reached. To add more channels, you need to
//		delete some channels or upgrade	to a <a href="/account"> better plan</a>.',
	'plan_channel_limit_reached' => 'The limit of channels (:channels) for the
		:plan_label plan has been reached. To add more channels, you need to
		delete some channels',
	'youtube_channel_deleted' => 'Channel deleted with success.',
	'youtube_channel_deleted_fail' => 'There was a problem deleting the
		channel.',
	'edit_account_text' => 'Edit your account details',
	'update_account' => 'update',
	'your_plan' => 'Plan',
	'signed_plan' => 'Signed plan:',
	'plan_videos' => 'You can download the last :video videos of the channels',
	'plan_all_videos' => 'You can download all videos of the channels',
	'plan_channels' => 'You can sign up to :channel channels',
	'plan_all_channels' => 'You can sign up to as many channels you want',
);
