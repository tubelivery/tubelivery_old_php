<?php

/**
 * Português
 */
return array(
	'not_found_title' => 'Check the address!!!',
	'not_found_description' => 'There was an error generationg the podcast. Please,
		verify if the address is correct and that you haven\'t deleted it from your account',
	'beeing_created_title' => 'This podcast is beeing created!',
	'beeing_created_description' => 'We are fetching your podcast\'s detais.
		Depending on the number of videos, it may take a while.
		Please, try again in a few minutes',
	'downloading_videos_title' => 'We are downloading the videos!',
	'downloading_videos_description' => 'We are downloading your podcast\'s videos.
		Depending on the number of videos and the quality you choose, it may take a while.
		Please, try again in a few minutes',
	'generated_time' => 'Dynamic page generated in :execution_time seconds by Tubelivery: ',
	'generated' => ' - podcast generated by Tubelivery',
);
