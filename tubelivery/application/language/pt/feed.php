<?php

/**
 * Português
 */
return array(
	'not_found_title' => 'Verifique o endereço!!!',
	'not_found_description' => 'Houve um erro na geração do podcast. Verifique se o
		endereço está correto e se o podcast não foi apagado da sua conta',
	'beeing_created_title' => 'Este podcast está sendo criado!',
	'beeing_created_description' => 'Estamos pegando os detalhes do prodcast.
		Dependendo do número de vídeos, isso ainda pode levar um tempo.
		Por favor, tente novamente em alguns minutos',
	'downloading_videos_title' => 'Estamos baixando os vídeos!',
	'downloading_videos_description' => 'Estamos baixando os vídeos do podcast.
		Dependendo do número de vídeos e da qualidade escolhida, isso ainda pode levar um tempo.
		Por favor, tente novamente em alguns minutos',
	'generated_time' => 'Página gerada dinamicamente em :execution_time segundos por Tubelivery: ',
	'generated' => ' - podcast gerado pelo Tubelivery',
);
