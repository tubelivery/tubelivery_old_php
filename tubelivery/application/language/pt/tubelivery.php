<?php

/**
 * Português
 */
return array(
	'lang' => 'pt',
	'other_lang' => 'en',
	'locale' => 'pt_BR',
	'tubelivery_slogan' => 'Crie podcasts de canais do YouTube',
	'login_error' => 'Usuário ou senha incorretos',
	'welcome_back' => 'Bem vindo novamente, :name! Você sabia que, com o seu plano, você pode adicionar quantos canais quiser?',
	'menu_plans' => 'Planos',
	'menu_login' => 'entrar',
	'menu_signup' => 'assinar',
	'menu_channels' => 'Meus canais',
	'menu_account' => 'Minha conta',
	'menu_support' => 'suporte',
	'menu_logout' => 'sair',
	'name' => 'Apelido',
	'username' => 'Usuário',
	'email' => 'Email',
	'password' => 'Senha',
	'remember_me' => 'Lembrar de mim',
	'login' => 'Entrar com',
	'login_facebook' => 'Facebook',
	'signup' => 'Assinar com',
	'signup_facebook' => 'Facebook',
	'or_email' => 'ou email',
	'logout' => 'Sair',
	'channel' => 'Canal',
	'channels' => 'Canais',
	'account' => 'Conta',
	'add_channel' => 'Adicionar canal',
	'channel_name' => 'Nome ou URL do canal',
	'videos' => 'vídeos',
	'subscribers' => 'assinantes',
	'exibitions' => 'exibições',
	'view_on_yt' => 'Ver no YouTube',
	'open_podcast' => 'Abrir o Podcast',
	'open_in_itunes' => 'Abrir no iTunes',
	'delete_podcast' => 'Apagar podcast',
	'podcast_deletion' => 'Exclusão de podcast',
	'podcast_deletion_text' => 'Você tem certeza que quer excluir o podcast do
		canal ":label"? Dependendo do seu plano, essa ação não poderá ser
		desfeita!',
	'cancel' => 'Cancelar',
	'quality' => 'Qualidade',
	'footer_about' => 'Sobre',
	'footer_version' => 'Versões',
	'footer_terms' => 'Termos de uso',
	'copyright' => '© '. Config::get('application.copyright_year') .
	' Tubelivery',
	'youtube_unreachable' => 'Não conseguimos conectar ao YouTube. Por favor,
		tente novamente mais tarde.',
	'youtube_channel_not_found' => 'Não conseguimos encontrar o canal "
		:yt_username".',
	'youtube_channel_added' => 'Canal ":channel_label" adicionado.',
	'youtube_channel_already_in_user' => 'Você já assina o canal
		":channel_label".',
//	'plan_channel_limit_reached' => 'O limite de canais (:channels) do plano
//		:plan_label foi atingido. Para adicionar mais canais, você precisa
//		remover canais ou mudar para um	<a href="/account"> plano melhor</a>.',
	'plan_channel_limit_reached' => 'O limite de canais (:channels) do plano
		:plan_label foi atingido. Para adicionar mais canais, você precisa
		remover algum canal',
	'youtube_channel_deleted' => 'Canal excluido com sucesso.',
	'youtube_channel_deleted_fail' => 'Ocorreu um problema ao tentar remover o
		canal.',
	'edit_account_text' => 'Edite os dados da sua conta',
	'update_account' => 'atualizar',
	'your_plan' => 'Plano',
	'signed_plan' => 'Plano assinado:',
	'plan_videos' => 'Você pode baixar os último :video vídeos do canal',
	'plan_all_videos' => 'Você pode baixar todos os vídeos do canal',
	'plan_channels' => 'Você pode assinar até :channel canais',
	'plan_all_channels' => 'Você pode assinar quantos canais quiser',
);
