<?php

$home_page = HTML::link('/', 'página inicial');

return array(
	/**
	 * English
	 */
	'lost' => array(
		'Precisamos de um mapa',
		"Acho que nos perdemos.",
		'Fizemos uma curva errada.'
	),
	'problem' => array(
		'Ops…',
		'Foi mal!',
		'Caramba!'
	),
	'meaning_title' => 'O que isso significa?',
	'redirect' => "Talvez você queira ir para nossa $home_page?",
	'error_404' => "Erro no servidor: 404 (Não Encontrado)",
	'error_500' => "Erro no servidor: 500 (Erro Interno do Servidor)",
	'meaning_text_404' => "Não conseguimos encontrar a página nos nossos servidores.
		Sentimos muito por isso.",
	'meaning_text_500' => "Algo deu errado no nosso servidor enquanto tentávamos
		processar sua requisição. Sentimos muito por isso e estamos trabalhando
		muito para resolver isso o mais rápido possível."
);
