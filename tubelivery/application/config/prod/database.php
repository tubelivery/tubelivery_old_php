<?php

return array(
	/*
	  |--------------------------------------------------------------------------
	  | Database Query Logging
	  |--------------------------------------------------------------------------
	  |
	  | By default, the SQL, bindings, and execution time are logged in an array
	  | for you to review. They can be retrieved via the DB::profile() method.
	  | However, in some situations, you may want to disable logging for
	  | ultra high-volume database work. You can do so here.
	  |
	 */

	'profile' => TRUE,
	'connections' => array(
		'mysql' => array(
			'driver' => 'mysql',
			'host' => 'tubelivery-db.caur8mguk2fw.us-east-1.rds.amazonaws.com',
			'database' => 'tubelivery',
			'username' => 'tubelivery',
			'password' => '5tr0ng15mYars3n0',
			'charset' => 'utf8',
			'prefix' => '',
		),
	),
);