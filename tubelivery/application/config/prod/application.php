<?php

return array(
	'profiler' => FALSE,
	'timezone' => 'UTC',
	'language_names' => array('English', 'Português'),
);
