<?php

return array(
	'profiler' => TRUE,
	'timezone' => 'America/Sao_Paulo',
	'language_names' => array('English', 'Português'),
);
