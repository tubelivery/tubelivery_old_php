<?php

class Youtube_Dl_Task {

	/**
	 * U
	 * Get the channel's videos and insert them in the DB.
	 * This function sets the status columns in the channels table with one
	 * of these values:
	 * <ul>
	 * <li>0 - no execution made OR execution had an error</li>
	 * <li>1 - execution is running</li>
	 * <li>2 - execution finished with success</li>
	 * </ul>
	 * After that, it inserts all channel's videos to the download queue.
	 * @param Array $args with channel ID
	 */
	public function run($args)
	{
		$channel_id = $args[0];
		$quality_id = $args[1];
		Log::Youtube_Dl_Task("channel_id = $channel_id | quality_id = $quality_id");

		$channel = Channel::find($channel_id);
		$quality = Quality::find($quality_id);

		Log::Youtube_Dl_Task("channel state = $channel->channel_state");
		echo('channel state = ' . $channel->channel_state . '
				');

		//Verify if some other execution is in process
		if ($channel->channel_state == 1)
		{
			return;
		}

		//Verify if state is 0 = no execution made
		$original_channel_state = $channel->channel_state;
		if ($original_channel_state == 0 OR $original_channel_state == 2)
		{
			//Change to state 1 = Started to insert the videos in the videos table
			$channel->channel_state = 1;
			$channel->update($channel->get_key(), $channel->attributes);

			$inserted_videos = Youtube::insert_channel_videos($channel);
			// If the insertion of the videos didn't work, change the state back to 0 = no execution made
			if ($inserted_videos === FALSE)
			{
				$channel->channel_state = $original_channel_state;
				$channel->update($channel->get_key(), $channel->attributes);
				return;
			}
			Youtube::add_videos_to_queue($channel, $quality);

			//Change the state to 2 = All videos and paths inserted
			$channel->channel_state = 2;
			$channel->update($channel->get_key(), $channel->attributes);
		}
	}

}