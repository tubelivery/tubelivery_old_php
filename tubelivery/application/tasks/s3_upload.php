<?php

require path('app') . '/libraries/aws/vendor/autoload.php';

use Aws\Common\Aws;
use Aws\Common\Enum\Region;
use Aws\S3\Enum\CannedAcl;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

class S3_Upload_Task {

	public function run($args)
	{
		$db_path = $args[0];
		$save_path = $args[1];

		// Instantiate an S3 client
		$s3 = S3Client::factory(
						array(
							'key' => 'AKIAJCD77ZSTTII5KJFA',
							'secret' => '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',
							'region' => Region::US_EAST_1
						)
		);

		// Upload a publicly accessible file. File size, file type, and md5 hash are automatically calculated by the SDK
		try
		{
			Log::S3_Upload("Starting S3 upload of file $save_path");
			$s3->putObject(array(
				'Bucket' => 'media.tubelivery.com',
				'Key' => $db_path,
				'Body' => fopen($save_path, 'r'),
				'ACL' => CannedAcl::PUBLIC_READ
			));
			Log::S3_Upload("Finished uploading $save_path");

			Log::S3_Upload('Deleting file ' . $save_path);
			unlink($save_path);
			Log::S3_Upload('Deleted file ' . $save_path);
		}
		catch (S3Exception $e)
		{
			Log::S3_Upload("ERROR - The file was not uploaded.");
		}
	}

}