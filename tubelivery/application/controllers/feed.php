<?php

class Feed_Controller extends Controller {

	public function action_index($podcast_hash = '')
	{
		$start = (float) array_sum(explode(' ',microtime()));

		//Increase the time limit to avoid error 500
		set_time_limit(1200);

		$feed_data = Podcast::feed($podcast_hash);

		if($feed_data == 'not found')
		{
			return Response::make(View::make('feed.not_found', array('data' => $podcast_hash)), 200, array('Content-type' => 'text/xml'));
		}
		//TODO - remove
		elseif($feed_data == 'beeing created')
		{
			// Create the correct response
			return Response::make(View::make('feed.beeing_created', array('channel' => Channel::get_by_podcast_hash($podcast_hash))), 200, array('Content-type' => 'text/xml'));
		}
		elseif($feed_data == 'downloading videos')
		{
			// Create the correct response
			return Response::make(View::make('feed.downloading_videos', array('channel' => Channel::get_by_podcast_hash($podcast_hash))), 200, array('Content-type' => 'text/xml'));
		}
		else
		{
			$end = (float) array_sum(explode(' ',microtime()));
			$feed_data['execution_time'] = sprintf("%.4f", ($end - $start));

			// Response as XML
			return Response::make(View::make('feed.feed', $feed_data), 200, array('Content-type' => 'text/xml'));
//			return View::make('feed.feed', $feed_data);
		}

	}

}