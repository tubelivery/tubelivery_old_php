<?php

class Channels_Controller extends Base_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->filter('before', 'user_without_channels')->only(array('index', 'view'));
	}

	public function action_index()
	{
		$user = Auth::user();
		$channels = $user->channels;

		//TODO -> quality is worng in the view!!!
		$selected_channel = $user->channels[0];

		$data = array('user' => $user, 'channels' => $channels, 'selected_channel' => $selected_channel);

		$data['mixpanel_data'] = array(
			Mixpanel::identify($user->email),
			Mixpanel::track('Channel page'),
			Mixpanel::track(array('podcast view' => $selected_channel->yt_username)),
			Mixpanel::people_set(array('last viwed podcast' => $selected_channel->yt_username, 'language' => __('tubelivery.lang'))),
			Mixpanel::register(array('language' => __('tubelivery.lang'))),
		);

		return View::make('channels.index', $data);
	}

	/**
	 * Show the details of a signed channel
	 * @param String $podcast_hash
	 * @return View
	 */
	public function action_view($podcast_hash = '')
	{
		$user = Auth::user();
		$channels = $user->channels;

		//
		if ($podcast_hash === '')
		{
			return Redirect::to('channels/');
		}
		$selected_channel = Channel::get_by_podcast_hash($podcast_hash);

		$data = array('user' => $user, 'channels' => $channels, 'selected_channel' => $selected_channel);

		$data['mixpanel_data'] = array(
			Mixpanel::identify($user->email),
			Mixpanel::track('Channel page'),
			Mixpanel::track(array('podcast view' => $selected_channel->yt_username)),
			Mixpanel::people_set(array('last viwed podcast' => $selected_channel->yt_username, 'language' => __('tubelivery.lang'))),
			Mixpanel::register(array('language' => __('tubelivery.lang'))),
		);

		return View::make('channels.index', $data);
	}

	/**
	 * Add a YouTube Channel to the user's list
	 * If the channel is New, create in the DB, else, return the existing
	 * @return Redirect
	 */
	public function action_add()
	{
		$yt_username = Input::get('channel');
		$quality = Input::get('quality');
		$result = Channel::add($yt_username, $quality);

		// If channel is actualy added, redirect to the channel
		if (is_array($result) && $result[0] === TRUE)
		{
			$user = Auth::user();
			$user_id = $user->get_key();
			$podcast_hash = Channel::podcast_hash_by_yt_username($result[1], $user_id);
			return Redirect::to('channels/view/' . $podcast_hash)
							->with('result', $result[2])
							->with('mixpanel_data', array(
								Mixpanel::identify($user->email),
								Mixpanel::people_set(array('last added podcast' => $yt_username, 'quality' => $quality, 'language' => __('tubelivery.lang'))),
								Mixpanel::register(array('last added podcast' => $yt_username, 'quality' => $quality, 'plan videos' => $user->plan->videos, 'language' => __('tubelivery.lang'))),
								Mixpanel::people_increment('signed podcasts'),
								Mixpanel::track('added podcast'),
									)
			);

//			return View::make('channels.index', $data);
		}
		else
			return Redirect::to('channels')
							->with('result', $result);
	}

	/**
	 * Delete a YouTube Channel from the user's list
	 * @return Redirect
	 */
	public function action_delete($podcast_hash)
	{
		$channel = Channel::get_by_podcast_hash($podcast_hash);
		$channel_name = $channel->label;
		$result = Channel::delete_channel($podcast_hash);

		$user = Auth::user();

		return Redirect::to('channels')
						->with('result', $result)
						->with('mixpanel_data', array(
							Mixpanel::identify($user->email),
							Mixpanel::people_set(array('removed podcast' => $channel_name, 'language' => __('tubelivery.lang'))),
							Mixpanel::register(array('removed podcast' => $channel_name, 'plan videos' => $user->plan->videos, 'language' => __('tubelivery.lang'))),
							Mixpanel::people_increment(array('signed podcasts' => -1)),
							Mixpanel::track('deleted podcast'),
						));
	}

}