<?php

class Facebook_Controller extends Controller {

	public function action_index()
	{
		Log::debug('Facebook Index');
		if(is_null(Input::get('name')))
			return;

		$data = array(
			'name' => Input::get('name'),
			'email' => Input::get('email'),
			'facebook_id' => Input::get('id'),
			'facebook_name' => Input::get('name'),
			'facebook_first_name' => Input::get('first_name'),
			'facebook_last_name' => Input::get('last_name'),
			'facebook_link' => Input::get('link'),
			'facebook_username' => Input::get('username'),
			'facebook_gender' => Input::get('gender'),
			'facebook_timezone' => Input::get('timezone'),
			'facebook_locale' => Input::get('locale'),
			'facebook_updated_time' => Input::get('updated_time'),
		);
		$user = User::find(DB::table('users')
								->where('email', '=', $data['email'])
								->or_where('facebook_id', '=', $data['facebook_id'])
								->only('id'));
		Log::debug(DB::table('users')
						->where('email', '=', $data['email'])
						->or_where('facebook_id', '=', $data['facebook_id'])
						->only('id'));

		// User doesn't exist. Create a new one and Log in
		if (!isset($user))
		{
			//Create a random password
			$password = Str::random('10');
			$data['password'] = Hash::make($password);

			if ($user = User::create($data))
			{
				// Try to log the new user
				if (Auth::login($user->get_key()))
				{
					return 'login';
				}
			}
		}
		// User exists and email match. Update FB Data and Log in
		elseif ($user->email == $data['email'])
		{
			if (User::update($user->get_key(), $data))
			{
				// Try to log the new user
				if (Auth::login($user->get_key()))
				{
					return ("login");
				}
			}
		}
		// User exists but email doesn't match. Update Email and FB Data and Log in
		elseif ($user->facebook_id == $data['facebook_id'])
		{
			if (User::update($user->get_key(), $data))
			{
				// Try to log the new user
				if (Auth::login($user->get_key()))
				{
					return ("login");
				}
			}
		}
		return 'error';
	}

	public function action_sign($signed_request)
	{
		Log::debug('Facebook Sign: ' . $signed_request);

		if($signed_request == '')
			return 'signed_request is null';


		$secret = '9afc8b5e1d5b5af0f8245bafe61089c5';

		list($encoded_sig, $payload) = explode('.', $signed_request, 2);

		// decode the data
		$sig = $this->base64_url_decode($encoded_sig);
		$data = json_decode($this->base64_url_decode($payload), true);

		if (strtoupper($data['algorithm']) !== 'HMAC-SHA256')
		{
			error_log('Unknown algorithm. Expected HMAC-SHA256');
			return null;
		}

		// Adding the verification of the signed_request below
		$expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);

		if ($sig !== $expected_sig)
		{
			error_log('Bad Signed JSON signature!');
			return null;
		}

		return 'signed';
	}

	private function base64_url_decode($input)
	{
		return base64_decode(strtr($input, '-_', '+/'));
	}

}