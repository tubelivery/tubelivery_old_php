<?php

class Account_Controller extends Base_Controller {

	public function action_index()
	{
		if (Auth::check())
			return View::make('account.account');
		else
			return Redirect::to('login');
	}

}

?>
