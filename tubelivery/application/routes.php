<?php

/* Controllers Routes */

Route::controller('channels');
Route::controller('account');
Route::controller('facebook');

Route::get('feed/(:any)/(:any?)', 'feed@index');

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
  | breeze to setup your application using Laravel's RESTful routing and it
  | is perfectly suited for building large applications and simple APIs.
  |
  | Let's respond to a simple GET request to http://example.com/hello:
  |
  |		Route::get('hello', function()
  |		{
  |			return 'Hello World!';
  |		});
  |
  | You can even respond to more than one URI:
  |
  |		Route::post(array('hello', 'world'), function()
  |		{
  |			return 'Hello World!';
  |		});
  |
  | It's easy to allow URI wildcards using (:num) or (:any):
  |
  |		Route::put('hello/(:any)', function($name)
  |		{
  |			return "Welcome, $name.";
  |		});
  |
 */

Route::get('/', array('before' => 'auth_check', 'do' => function()
	{
		$mixpanel_data = array(
			Mixpanel::track('Landing page'),
			Mixpanel::register(array('language' => __('tubelivery.lang'))),
		);
		return Redirect::to('login')->with('mixpanel_data', $mixpanel_data);
	}));

Route::get('login', array('before' => 'auth_check', 'do' => function()
	{
		$mixpanel_data = array(
			'mixpanel_data' => array(
				Mixpanel::track('Log in page'),
				Mixpanel::register(array('language' => __('tubelivery.lang'))),
			)
		);
		return View::make('auth.login', $mixpanel_data);
	}));

Route::get('signup', array('before' => 'auth_check', 'do' => function()
	{
		$mixpanel_data = array(
			'mixpanel_data' => array(
				Mixpanel::track('Sign up page'),
				Mixpanel::register(array('language' => __('tubelivery.lang'))),
			)
		);
		return View::make('auth.signup', $mixpanel_data);
	}));

Route::post('login', array('before' => 'csrf', function()
	{
		$data = array(
			'username' => Input::get('email'),
			'password' => Input::get('password'),
			'remember' => Input::get('remember') === 'true' ? TRUE : FALSE
		);

		if (Auth::attempt($data))
		{
			Log::info('User ' . Auth::user()->email . ' has logged in');
			// Goes to the page requested before the login
			if (Session::has('pre_login_url'))
			{
				$url = Session::get('pre_login_url');
				Session::forget('pre_login_url');
				return Redirect::to($url);
			}
			else
			{
				return Redirect::to('channels')
								->with(
										'result', __('tubelivery.welcome_back', array('name' => Auth::user()->name)
										)
								)->with('mixpanel_data', array(
							Mixpanel::identify(Input::get('email')),
							Mixpanel::people_set(array('language' => __('tubelivery.lang'))),
							Mixpanel::track('User loged in'),
							Mixpanel::register(array('language' => __('tubelivery.lang'))),
								)
				);
			}
		}
		else
		{
			Input::flash('except', array('password'));
			return Redirect::to('login')
							->with_input('except', array('password'))
							->with('login_errors', true);
		}
	}));

Route::post('user_update', array('before' => 'csrf', function()
	{
		//TODO - check old password to alow changes
		$user_id = Auth::user()->get_key();
		$data = array(
			'name' => Input::get('name'),
//			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		// validation rules
		$rules = array(
			'name' => 'required|between:1,255',
//			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:6'
		);

		// create a validator
		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			// redirect back to form with
			// error messages and post data
			Input::flash();
			return Redirect::to('account')
							->with_input('except', array('password'))
							->with_errors($validator);
		}

		$data['password'] = Hash::make(Input::get('password'));

		if (User::update($user_id, $data))
		{
//			$login_data = array(
//				'username' => Input::get('email'),
//				'password' => Input::get('password')
//			);
//
			// Try to log the new user
//			if (Auth::attempt($login_data))
			return Redirect::to('account');
		}
	}));

Route::post('signup', array('before' => 'csrf', function()
	{
		$data = array(
			'name' => Input::get('name'),
			'email' => Input::get('email'),
			'password' => Input::get('password')
		);
		// validation rules
		$rules = array(
			'name' => 'required|between:1,255',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:6'
		);

		// create a validator
		$validator = Validator::make($data, $rules);

		if ($validator->fails())
		{
			// redirect back to form with
			// error messages and post data
			Input::flash();
			return Redirect::to('signup')
							->with_input('except', array('password'))
							->with_errors($validator);
		}
		$data['password'] = Hash::make(Input::get('password'));

		// Try to create the user
		if (User::create($data))
		{
			$login_data = array(
				'username' => Input::get('email'),
				'password' => Input::get('password')
			);

			// Try to log the new user
			if (Auth::attempt($login_data))
			{
				$plan = Auth::user()->plan;
				return Redirect::to('channels')->with('mixpanel_data', array(
							Mixpanel::alias(Input::get('email')),
							Mixpanel::identify(Input::get('email')),
							Mixpanel::people_set(array('language' => __('tubelivery.lang')), date("Y-m-d H:i:s"), Input::get('email'), NULL, NULL, Input::get('name'), Input::get('email')),
							Mixpanel::register(array('plan' => $plan->label, 'videos' => $plan->videos, 'channels' => $plan->channels, 'language' => __('tubelivery.lang'))),
							Mixpanel::track('User signed up'),
								)
				);
			}
		}

		//If some error ocour, redirect with errors
		Input::flash('except', array('password'));
		return Redirect::to('signup')
						->with_input()
						->with('signup_errors', true);
	}));

Route::get('logout', function()
		{
			if (Auth::check())
				Log::info('User ' . Auth::user()->email . ' has logged out');
			Auth::logout();
			return Redirect::to('http://tubelivery.com/' . __('tubelivery.lang') . '/');
		});

Route::get('fb_channel', function()
		{
			return View::make('auth.fb_channel');
		});
/*
  |--------------------------------------------------------------------------
  | Application 404 & 500 Error Handlers
  |--------------------------------------------------------------------------
  |
  | To centralize and simplify 404 handling, Laravel uses an awesome event
  | system to retrieve the response. Feel free to modify this function to
  | your tastes and the needs of your application.
  |
  | Similarly, we use an event to handle the display of 500 level errors
  | within the application. These errors are fired when there is an
  | uncaught exception thrown in the application.
  |
 */

Event::listen('404', function()
		{
			return Response::error('404');
		});

Event::listen('500', function()
		{
			return Response::error('500');
		});

/*
  |--------------------------------------------------------------------------
  | Route Filters
  |--------------------------------------------------------------------------
  |
  | Filters provide a convenient method for attaching functionality to your
  | routes. The built-in before and after filters are called before and
  | after every request to your application, and you may even create
  | other filters that can be attached to individual routes.
  |
  | Let's walk through an example...
  |
  | First, define a filter:
  |
  |		Route::filter('filter', function()
  |		{
  |			return 'Filtered!';
  |		});
  |
  | Next, attach the filter to a route:
  |
  |		Route::get('/', array('before' => 'filter', function()
  |		{
  |			return 'Hello World!';
  |		}));
  |
 */

Route::filter('auth_check', function()
		{
			if (Auth::check())
				return Redirect::to('channels');
		});

Route::filter('auth_guest', function()
		{
			if (Auth::guest())
			{
				// Save the attempted URL
				Session::put('pre_login_url', URI::current());

				return Redirect::to('login');
			}
		});

Route::filter('user_without_channels', function()
		{
			if (sizeof(Auth::user()->channels) == 0)
			{
				$data = array('user' => Auth::user());
				$data['mixpanel_data'] = array(
					Mixpanel::identify(Auth::user()->email),
					Mixpanel::track('Channel page'),
					Mixpanel::register(array('language' => __('tubelivery.lang'))),
					Mixpanel::people_set(array('language' => __('tubelivery.lang'))),
				);

				return View::make('channels.index', $data);
			}
		});

Route::filter('before', function()
		{
			// Do stuff before every request to your application...
		});

Route::filter('after', function($response)
		{
			// Do stuff after every request to your application...
		});

Route::filter('csrf', function()
		{
			if (Request::forged())
				return Response::error('500');
		});