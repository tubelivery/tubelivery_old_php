<?php

class Plan extends Eloquent {

	public $includes = array('qualities');

	public function users()
	{
		return $this->has_many('User');
	}

	public function qualities()
	{
		return $this->has_many_and_belongs_to('Quality', 'quality_plan');
	}

}

/* End of file plan.php */
/* Location: ./application/controllers/plan.php */