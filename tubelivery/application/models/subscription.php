<?php

class Subscription extends Eloquent {

	public function user()
	{
		return $this->belongs_to('User', 'user_id');
	}

}