<?php

class Quality extends Eloquent {

	public function users()
	{
		return $this->has_many_and_belongs_to('User', 'channel_user');
	}

	public function plans()
	{
		return $this->has_many_and_belongs_to('Plan', 'quality_plan');
	}

	public function videos()
	{
		return $this->has_many_and_belongs_to('Video', 'quality_video');
	}

	/**
	 * U
	 * Verify if user can use the quality that came from the form. This avoid
	 * users setting the quality via BugZilla
	 * @param User $user The User model
	 * @param int $quality_id The quality ID that came from the form radio
	 */
	public static function update_to_allowed_quality($user, $quality_id)
	{
		// Get the User allowed qualities from it's plan
		$allowed_qualities = $user->plan->qualities()->get();

		foreach ($allowed_qualities as $allowed_quality)
		{
			if ($allowed_quality->get_key() == $quality_id)
				return $quality_id;
		}
		Log::info("User $user->email tryed to change the quality to $quality_id via BugZila");
		return 1;
	}

}