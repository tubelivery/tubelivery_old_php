<?php

class Channel extends Eloquent {

	public $includes = array('qualities');

	public function __construct()
	{
		parent::__construct();
		return $this;
	}

	public function users()
	{
		return $this->has_many_and_belongs_to('User', 'channel_user_quality');
	}

	public function qualities()
	{
		return $this->has_many_and_belongs_to('Quality', 'channel_user_quality');
	}

	public function videos()
	{
		return $this->has_many('Video');
	}

	/**
	 * U
	 * Add a channel to an user.
	 * Option 1: channel doesn't exist: return error
	 * Option 2: channel exists, but is not on DB: create channel on DB and add the relationship
	 * Option 3: channel exists and is on DB: update channel videos on DB and add the relationship
	 * @param String $yt_username - Channel Username or URL
	 * @param Integer $quality_id - id from qualities table
	 * @return String message that will be treated in the controller and passed to the view
	 */
	public static function add($yt_username, $quality_id)
	{
		$user = Auth::user();

		// Verify if user plan allow more channels
		if (($user->plan->channels != 0) && ($user->plan->channels <= sizeof($user->channels)))
		{
			return __('tubelivery.plan_channel_limit_reached', array('plan_label' => $user->plan->label, 'channels' => $user->plan->channels));
		}

		// Try to reach YouTube
		if (Youtube::is_unreachable())
		{
			Log::error("YouTube unreachable: $errstr ($errno)");
			return __('tubelivery.youtube_unreachable');
		}

		$yt_username = Youtube::clean_username($yt_username);

		// Verify that the channle exists
		if (YouTube::channel_does_not_exist($yt_username))
		{
			Log::info("Channel $yt_username not found");
			return __('tubelivery.youtube_channel_not_found', array('yt_username' => $yt_username));
		}

		// Verify if the channel is already in the DB
		$channel = Channel::where('yt_username', '=', $yt_username)->first();

		$attributes = YouTube::get_channel_attributes($yt_username);

		$quality_id = Quality::update_to_allowed_quality($user, $quality_id);

		// Channel is not in the DB. Create the channel and the relationship
		if ($channel === NULL)
		{
			// Create the Channel and store on DB
			$channel = Channel::create($attributes);

			//Create the relationship between user, channel and quality in "channel_user_quality" table
			DB::table('channel_user_quality')
					->insert(
							array(
								'user_id' => $user->get_key(),
								'channel_id' => $channel->get_key(),
								'quality_id' => $quality_id,
								'podcast_hash' => Podcast::create_podcast_unique_hash($user->get_key(), $channel->get_key()),
								'created_at' => new \DateTime,
								'updated_at' => new \DateTime,
					));

			static::update_channel($channel, $attributes);
			Youtube::save_channel_videos_in_bg($channel, $quality_id);

			return array(
				TRUE,
				$yt_username,
				__('tubelivery.youtube_channel_added', array('channel_label' => $attributes['label'])
				)
			);
		}
		// Channel is in the DB. Update channel data
		else
		{
			// User already has the channel
			if (User::has_channel($user, $channel))
			{
				static::update_channel($channel, $attributes);
				Youtube::save_channel_videos_in_bg($channel, $quality_id);

				return __('tubelivery.youtube_channel_already_in_user', array('channel_label' => $channel->label));
			}
			// User does not have the channel
			else
			{
				//Create the relationship between user, channel and quality in "channel_user_quality" table
				$user->channels()->attach($channel, array('quality_id' => $quality_id, 'podcast_hash' => Podcast::create_podcast_unique_hash($user->get_key(), $channel->get_key())));
				Youtube::save_channel_videos_in_bg($channel, $quality_id);

				return array(
					TRUE,
					$yt_username,
					__('tubelivery.youtube_channel_added', array('channel_label' => $channel->label)
					)
				);
			}
		}

		return NULL;
	}

	/**
	 * U
	 * Update the Channel details and the number of videos
	 * @param Channel $channel
	 * @param array $attributes for the Channel object
	 * @return boolean True if new videos were added, False otherwise
	 */
	public static function update_channel($channel, $attributes = '')
	{
		if ($attributes === '')
			$attributes = YouTube::get_channel_attributes($channel->yt_username);

		// Update the DB
		$channel->update($channel->get_key(), $attributes);
		return Channel::find($channel->get_key());
	}

	/**
	 * U
	 * Remove a channel to an user.
	 * Option 1: channel doesn't exist: return error
	 * Option 2: channel exists, but is not on DB: create channel on DB and add the relationship
	 * Option 3: channel exists and is on DB: update channel videos on DB and add the relationship
	 * @param String $podcast_hash The podcast hash
	 * @return String Message to be passed to the view
	 */
	public static function delete_channel($podcast_hash)
	{
		$user_id = Auth::user()->get_key();
		if (DB::table('channel_user_quality')
						->where('podcast_hash', '=', $podcast_hash)
						->where('user_id', '=', $user_id)
						->delete() === 1)
			return __('tubelivery.youtube_channel_deleted');
		else
			return __('tubelivery.youtube_channel_deleted_fail');
	}

	/**
	 * Return the podcast hash for this channel for a user
	 * @param User $user The user that has signed for the channel
	 * @return String The podcast hash
	 */
	public function podcast_hash($user = NULL)
	{
		//TODO - verify if this is really used
		$channel_id = $this->get_key();
		$user_id = $user === NULL ? Auth::user()->get_key() : $user->get_key();
		return DB::table('channel_user_quality')
						->where('channel_id', '=', $channel_id)
						->where('user_id', '=', $user_id)
						->only('podcast_hash');
	}

	/**
	 * Return the podcast hash for a channel for a user
	 * @param String $yt_username The YouTube channel name
	 * @param Integer $user_id The User id
	 * @return String The podcast hash
	 */
	public static function podcast_hash_by_yt_username($yt_username, $user_id)
	{
		//TODO - verify if this is really used
		return DB::table('channel_user_quality')
						->join('channels', 'channels.id', '=', 'channel_user_quality.channel_id')
						->where('channels.yt_username', '=', $yt_username)
						->where('channel_user_quality.user_id', '=', $user_id)
						->only('podcast_hash');
	}

	/**
	 * U
	 * Get a Channel Object if $podcast_hash exists
	 * @param String $podcast_hash
	 * @return Mixed Channel or FALSE, if the $podcast_hash is not found
	 */
	public static function get_by_podcast_hash($podcast_hash)
	{
		return Channel::find(DB::table('channels')
								->join('channel_user_quality', 'channels.id', '=', 'channel_user_quality.channel_id')
								->where('channel_user_quality.podcast_hash', '=', $podcast_hash)
								->only('channels.id'));
	}

	/**
	 * Get all available qualities for each video in a channel
	 * @param Channel $channel A channel object
	 * @return Matrix Matrix in the format<br/>
	 * array('youtube_video_id' => array('quality_id' => TRUE))
	 */
	public static function get_channel_videos_available_qualities($channel)
	{
		$command = "./application/libraries/youtube-dl -iF ytuser:$channel->yt_username";
		$output = array();
		$return_var = NULL;
		$videos = array();

		Log::debug("exec($command)");

		exec($command, $output, $return_var);

		Log::debug("get_channel_videos_available_qualities($channel->label)");

		$yt_username = NULL;
		$youtube_video_id = NULL;

		$qualities = Quality::all();

		/*
		 * Create an array in the format array('$youtube_video_id' => 'video_id')
		 */
		// Create an array with all channel's videos
		$channel_videos = DB::table('videos')
				->where('channel_id', '=', $channel->get_key())
				->get(array('youtube_video_id', 'id'));

		$videos_ids = array();
		foreach ($channel_videos as $video)
		{
			$videos_ids[$video->youtube_video_id] = $video->id;
		}


		//Process each line
		foreach ($output as $line)
		{
			Log::debug($line);
			// Try to find "Extracting video information" in line
			$a = explode(" ", $line);
			$end = count($a) - 1;
			$value = $a[$end];
			$begin = $a[0];

			Log::debug($value);

			// Check if the line matches the "Extracting video information tag
			if ($value == 'information')
			{
				//Remove the ":" from the youtube_video_id
				$youtube_video_id = rtrim($a[1], ":");
			}

			// Check if the line does not have the "[youtube]" AND "Available" tags
			elseif ($begin != '[youtube]' && $begin != 'Available')
			{
				foreach ($qualities as $quality)
				{
					if ($line == $quality->youtube_dl_format)
					{
						$videos[] = array(
							'video_id' => $videos_ids[$youtube_video_id],
							'quality_id' => $quality->get_key(),
							'created_at' => new \DateTime,
							'updated_at' => new \DateTime);
					}
				}
			}
		}
		DB::table('quality_video')->insert($videos);
//		return $videos;
	}

	public static function find_by_id($id)
	{
		return Channel::find($id);
	}

	public function save_me()
	{
		return $this->save();
	}

	public static function channel_download_state_offset_for_quality($quality)
	{
		return ($quality->get_key() - 1) * 2;
	}

}