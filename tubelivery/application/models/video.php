<?php

class Video extends Eloquent {

	public $includes = array('channel');

	public function path()
	{
		return $this->has_many('Path');
	}

	public function channel()
	{
		return $this->belongs_to('Channel');
	}
}