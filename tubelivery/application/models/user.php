<?php

class User extends Eloquent {

	public $includes = array('plan', 'channels');
	public static $hidden = array('password');

	public function channels()
	{
		return $this->has_many_and_belongs_to('Channel', 'channel_user_quality')->order_by('label', 'asc')->with('quality_id');
	}

	public function plan()
	{
		return $this->belongs_to('Plan');
	}

	/**
	 * Get a User Object if $podcast_hash exists
	 * @param String $podcast_hash
	 * @return User May also return FALSE, if the $podcast_hash is not found
	 */
	public static function get_by_podcast_hash($podcast_hash)
	{
		return User::find(DB::table('users')
								->join('channel_user_quality', 'users.id', '=', 'channel_user_quality.user_id')
								->where('channel_user_quality.podcast_hash', '=', $podcast_hash)
								->only('users.id'));
	}

	/**
	 * U
	 * Verify if a user has already signed for a channel
	 * @param User $user The User object
	 * @param Channel $channel The Channel object
	 * @return boolean TRUE if user has signed for the channel, FALSE otherwise
	 */
	public static function has_channel($user, $channel)
	{
		if (DB::table('channel_user_quality')
						->where('channel_id', '=', $channel->get_key())
						->where('user_id', '=', $user->get_key())
						->first() !== NULL)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}