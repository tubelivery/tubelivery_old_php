<?php

class Path extends Eloquent {

	public function path()
	{
		return $this->belongs_to('Video');
	}

	/**
	 * U
	 * @param Quality $quality The quality object
	 * @param array $video_ids Array of int with video ids
	 * @return Array Array of Path Objects qith the already inserted
	 */
	public static function get_inserted_paths($quality, $video_ids)
	{
		Log::get_inserted_paths("qualidade = " . $quality->label);
		return Path::where('quality_id', '=', $quality->get_key())
						->where_in('video_id', $video_ids)
						->get();
	}

	/**
	 * U
	 * Return the status of a video to be inserted according to it's number
	 * @param int $video_number The counter used to insert new videos to the queue
	 * @return int The status
	 */
	public static function get_status($video_number)
	{
		if ($video_number <= 30)
			return 1;
		elseif ($video_number <= 60)
			return 3;
		elseif ($video_number <= 120)
			return 5;
		elseif ($video_number <= 240)
			return 7;
		elseif ($video_number <= 1000)
			return 9;
	}

	/**
	 * Get all the videos tha has already been downloaded
	 * @param Channel $channel The channel object
	 * @param Quality $quality The quality object
	 * @return array Array with 'channel' and array of podcast items
	 */
	public static function get_downloaded_videos($channel, $quality, $user)
	{
		// Retrieve the videos from the channel, limited by user's plan
		$limit = $user->plan->videos == 0 ? 1000 : $user->plan->videos;

		$items = DB::table('paths AS p')
				->join('videos AS v', 'p.video_id', '=', 'v.id')
				->join('channels AS c', 'v.channel_id', '=', 'c.id')
				->where('p.status', '=', 0)
				->where('p.quality_id', '=', $quality->get_key())
				->where('c.id', '=', $channel->get_key())
				->order_by('v.pub_date', 'DESC')
				->get(array('p.path', 'p.file_size', 'v.youtube_video_id',
			'v.label', 'v.description', 'v.duration',
			'v.pub_date', 'v.author', 'v.icon_url'));
		if (count($items) == 0)
			return 'downloading videos';
		else
			return array('channel' => $channel, 'items' => $items);
	}

}