<?= '<?xml version="1.0" encoding="UTF-8"?>
' ?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
	<channel>
		<title>{{ $channel->label }} - {{ __('feed.downloading_videos_title') }}</title>
		<link>http://app.tubelivery.com/{{ __('tubelivery.lang') }}/{{ URI::current() }}</link>
		<copyright>©{{ Config::get('application.copyright_year') }}, Tubelivery</copyright>
		<description>{{ Podcast::xml_scape($channel->description) }}</description>
		<itunes:subtitle>{{ Podcast::xml_scape($channel->description) }}</itunes:subtitle>
		<itunes:summary>{{ Podcast::xml_scape($channel->description) }}</itunes:summary>
		<generator>Tubelivery</generator>
		<itunes:category text="TV &amp; Film"></itunes:category>
		<itunes:keywords>YouTube, video, {{ Podcast::xml_scape($channel->label) }}</itunes:keywords>
		<itunes:author>{{ Podcast::xml_scape($channel->label) }}</itunes:author>
		<managingEditor>tubelivery@tubelivery.com</managingEditor>
		<itunes:owner>
			<itunes:name>Tubelivery</itunes:name>
			<itunes:email>tubelivery@tubelivery.com</itunes:email>
		</itunes:owner>
		<itunes:image href="{{ $channel->icon_url }}"></itunes:image>
		<itunes:explicit>no</itunes:explicit>
		<language>{{ __('tubelivery.lang') }}</language>
	</channel>
</rss>