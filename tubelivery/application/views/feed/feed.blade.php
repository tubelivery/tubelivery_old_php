<?= '<?xml version="1.0" encoding="UTF-8"?>
' ?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
	<channel>
		<title>{{ $channel->label }}</title>
		<link>http://app.tubelivery.com/{{ __('tubelivery.lang') }}/{{ URI::current() }}</link>
		<copyright>©{{ Config::get('application.copyright_year') }}, Tubelivery</copyright>
		<description>{{ Podcast::xml_scape($channel->description) }}</description>
		<itunes:subtitle>{{ Podcast::xml_scape($channel->label) }} {{ __('feed.generated') }}</itunes:subtitle>
		<itunes:summary>{{ Podcast::xml_scape($channel->description) }}</itunes:summary>
		<generator>Tubelivery</generator>
		<itunes:category text="TV &amp; Film"></itunes:category>
		<itunes:keywords>YouTube, video, {{ Podcast::xml_scape($channel->label) }}</itunes:keywords>
		<itunes:author>{{ Podcast::xml_scape($channel->label) }}</itunes:author>
		<managingEditor>tubelivery@tubelivery.com (Tubelivery) </managingEditor>
		<itunes:owner>
			<itunes:name>Tubelivery</itunes:name>
			<itunes:email>tubelivery@tubelivery.com</itunes:email>
		</itunes:owner>
		<itunes:image href="{{ $channel->icon_url }}"></itunes:image>
		<itunes:explicit>no</itunes:explicit>
		<language>{{ __('tubelivery.lang') }}</language>
@foreach($items as $item)
		<item>
			<title>{{ Podcast::xml_scape($item->label) }}</title>
			<itunes:author>{{ Podcast::xml_scape($item->author) }}</itunes:author>
			<description>{{ Podcast::xml_scape($item->description) }}</description>
			<itunes:subtitle>{{ Podcast::xml_scape($item->description) }}</itunes:subtitle>
			<itunes:summary>{{ Podcast::xml_scape($item->description) }}</itunes:summary>
			<link>http://tubelivery.com</link>
			<enclosure url="http://media.tubelivery.com/{{ $item->path }}" length="{{ $item->file_size }}" type="video/mp4"></enclosure>
			<pubDate>{{ gmdate(DATE_RSS, strtotime($item->pub_date)) }}</pubDate>
			<itunes:duration>{{ $item->duration }}</itunes:duration>
			<itunes:explicit>no</itunes:explicit>
			<itunes:image href="{{ $item->icon_url }}"></itunes:image>
		</item>
@endforeach
	</channel>
</rss>
<!-- {{ __('feed.generated_time', array('execution_time' => $execution_time)).__('tubelivery.tubelivery_slogan') }} -->
