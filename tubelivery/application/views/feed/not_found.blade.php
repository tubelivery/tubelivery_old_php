<?= '<?xml version="1.0" encoding="UTF-8"?>
' ?>
<rss xmlns:itunes="http://www.itunes.com/dtds/podcast-1.0.dtd" version="2.0">
	<channel>
		<title>Tubelivery - {{ __('feed.not_found_title') }}</title>
		<link>http://app.tubelivery.com/{{ __('tubelivery.lang') }}/{{ URI::current() }}</link>
		<copyright>©{{ Config::get('application.copyright_year') }}, Tubelivery</copyright>
		<description>{{ __('feed.not_found_description') }}</description>
		<itunes:subtitle>{{ __('feed.not_found_description') }}</itunes:subtitle>
		<itunes:summary>{{ __('feed.not_found_description') }}</itunes:summary>
		<generator>Tubelivery</generator>
		<itunes:category text="TV &amp; Film"></itunes:category>
		<itunes:keywords>YouTube, video</itunes:keywords>
		<itunes:author>Tubelivery</itunes:author>
		<managingEditor>tubelivery@tubelivery.com</managingEditor>
		<itunes:owner>
			<itunes:name>Tubelivery</itunes:name>
			<itunes:email>tubelivery@tubelivery.com</itunes:email>
		</itunes:owner>
		<itunes:image href="http://tubelivery.com/wp-content/uploads/2013/01/logo.png"></itunes:image>
		<itunes:explicit>no</itunes:explicit>
		<language>{{ __('tubelivery.lang') }}</language>
	</channel>
</rss>