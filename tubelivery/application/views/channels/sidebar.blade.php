@if (Session::has('result'))
<div id="fade-alert" class="alert alert-info span6 offset2 fade">
	<button type="button" class="close" data-dismiss="alert">&times;</button>
	<p data-dismiss="alert">{{ Session::get('result') }}</p>
</div>
@endif
<div id="container" class="row-fluid">
	<div class="span12">
		<div class="bs-docs-sidebar span3">
			<ul class="nav nav-list bs-docs-sidenav">
				<li>
					<h3>{{ __('tubelivery.channels') }}</h3>
				</li>
				@if (isset($channels))
				<?
				$in = '';
				$autofocus = FALSE;
				?>
				@foreach ($channels as $channel)
				<?
				$class = '';
				if ($channel->get_key() == $selected_channel->get_key())
					$class = 'active';
				?>
				<li class="{{ $class }}">
					{{ HTML::link('channels/view/' . $channel->podcast_hash($user), '<i class="icon-chevron-right"></i>' . $channel->label, array('html' => TRUE)) }}
				</li>
				@endforeach
				@else
				<?
				$in = ' in';
//				$autofocus = array('autofocus' => 'autofocus');
				$autofocus = TRUE;
				?>
				@endif
				<li>
					<a id="channel-add-toggle" href="#" data-toggle="collapse" data-target="#add-channel">
						<i class="icon-plus-sign"></i>
						{{ __('tubelivery.add_channel') }}
					</a>
					<div id="add-channel" class="collapse{{ $in }}">
						<div id="add-channel-form">
							{{ Form::open('channels/add') }}
							@if($autofocus)
							{{ Form::text('channel', Input::old('channel'), array('id'=> 'channel-add-field', 'placeholder' => __('tubelivery.channel_name'), 'autofocus' => 'autofocus')) }}
							@else
							{{ Form::text('channel', Input::old('channel'), array('id'=> 'channel-add-field', 'placeholder' => __('tubelivery.channel_name'))) }}
							@endif
							{{ Form::label('quality', __('tubelivery.quality')) }}
							<div class="btn-group row-fluid quality-toggle" data-toggle-name="quality" data-toggle="buttons-radio" >
								<?
								foreach ($user->plan->qualities as $i => $quality)
								{
									?>
									<button type="button" value="{{ $quality->id }}" class="btn btn-mini {{ $i == 0 ? 'active' : ''}}" data-toggle="button">{{ $quality->label }}</button>
									<?
								}
								?>
							</div>
							<input type="hidden" name="quality" id="quality" value="1" />
							<div class="row-fluid">
								{{ Form::submit(__('tubelivery.add_channel'), array('class' => 'btn')) }}
							</div>
							{{ Form::close() }}
						</div>
					</div>
				</li>
			</ul>
		</div>