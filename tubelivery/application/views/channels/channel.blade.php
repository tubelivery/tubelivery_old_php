@if (isset($selected_channel))
<div id="content" class="span9 main row-fluid">
	<div class="row-fluid">
		<div id="channel-left-content" class="span4">
			<div class="channel-image row span11">
				{{ HTML::image($selected_channel->icon_url, $selected_channel->label)}}
			</div>
			<div class="row-fluid btn-group btn-group-vertical span11">
				<a class="btn btn-primary span*" href="{{ 'http://app.tubelivery.com/' . __('tubelivery.lang') . '/feed/' . $selected_channel->podcast_hash($user) . '/' }}">
					{{ __('tubelivery.open_podcast') }}
				</a>
				<a class="btn btn-primary span*" href="{{ 'itpc://app.tubelivery.com/' . __('tubelivery.lang') . '/feed/' . $selected_channel->podcast_hash($user) . '/' }}">
					{{ __('tubelivery.open_in_itunes') }}
				</a>
				<a class="btn btn-info span*" href="{{ 'http://www.youtube.com/user/' . $selected_channel->yt_username . '/' }}" target="_blank">
					{{ __('tubelivery.view_on_yt') }}
				</a>
				<a href="#deleteModal" class="btn btn-danger span*" data-toggle="modal">
					{{ __('tubelivery.delete_podcast') }}
				</a>
			</div>
		</div>
		<div id="channel-main-content" class="span8">
			<div id="channel-label" class="row-fluid">
				<h1>{{ $selected_channel->label }}</h1>
			</div>
			<div id="channel-description" class="row-fluid">
				{{ nl2br($selected_channel->description) }}
			</div>
		</div>
	</div>
</div>
<div id="deleteModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">{{ __('tubelivery.podcast_deletion') }}</h3>
	</div>
	<div class="modal-body">
		<p>{{ __('tubelivery.podcast_deletion_text', array('label' => $selected_channel->label)) }}</p>
	</div>
	<div class="modal-footer">
		{{ HTML::link('/channels/delete/' . $selected_channel->podcast_hash($user) . '/', __('tubelivery.delete_podcast'), array('role' => 'button', 'class' => 'btn btn-danger', 'data-toggle' => 'modal')) }}
		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">{{ __('tubelivery.cancel') }}</button>
	</div>
</div>

@endif
</div>
</div>