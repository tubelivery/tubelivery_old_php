@layout('templates.base')
@section('content')
<div id="content" class="span6 offset3">
	<div class="main">
		<?php $lost = array(__('html-error.lost.0'), __('html-error.lost.1'), __('html-error.lost.2')); ?>

		<h1>{{ $lost[mt_rand(0, 2)] }}</h1>

		<h2>{{ __('html-error.error_404') }}</h2>

		<hr>

		<h3>{{ __('html-error.meaning_title') }}</h3>

		<p>{{ __('html-error.meaning_text_404') }}</p>

		<p>{{ __('html-error.redirect') }}</p>
	</div>
</div>
@endsection