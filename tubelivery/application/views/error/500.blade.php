@layout('templates.base')
@section('content')
<div id="content" class="span6 offset3">
	<div class="main">
		<?php $problem = array(__('html-error.problem.0'), __('html-error.problem.1'), __('html-error.problem.2')); ?>

		<h1>{{ $problem[mt_rand(0, 2)] }}</h1>

		<h2>{{ __('html-error.error_500') }}</h2>

		<hr>

		<h3>{{ __('html-error.meaning_title') }}</h3>

		<p>{{ __('html-error.meaning_text_500') }}</p>

		<p>{{ __('html-error.redirect') }}</p>
	</div>
</div>
@endsection