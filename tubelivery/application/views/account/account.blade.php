@layout('templates.base')
@section('content')
<?
$user = Auth::user();
$label = array('class' => 'control-label');
$input = array('class' => 'span8');
?>
<div id="content" class="span4 offset4 main row-fluid">
	<h4>
		{{ __('tubelivery.edit_account_text') }}
	</h4>
	<div class="row-fluid">
		{{ Form::open('user_update', 'POST') }}
		@if (Session::has('signup_errors'))
		<span class="error">Username or password incorrect.</span>
		@endif
		@if ($errors->has())
		<div class="alert alert-error">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			@foreach($errors->all() as $message)
			<p>{{ $message }}</p>
			@endforeach
		</div>
		@endif
		<div class="input-prepend row-fluid">
			<span class="add-on account-detail">{{ __('tubelivery.name') }}</span>
			{{ Form::text('name', $user->name, $input) }}
		</div>
		<div class="input-prepend row-fluid">
			<span class="add-on account-detail">{{ __('tubelivery.email') }}</span>
			{{ Form::text('email', $user->email, array('class' => 'span8', 'readonly')) }}
		</div>
		<div class="input-prepend row-fluid">
			<span class="add-on account-detail">{{ __('tubelivery.password') }}</span>
			{{ Form::password('password', $input) }}
		</div>
		<div class="controls">
			{{ Form::submit(__('tubelivery.update_account'), array('class' => 'btn')) }}
		</div>
		{{ Form::token() }}
		{{ Form::close() }}
	</div>
	<div class="row-fluid">
		<h4>{{ __('tubelivery.your_plan') }}</h4>
		<p>{{ __('tubelivery.signed_plan') }} {{ $user->plan->label }}</p>
		<p>
			@if($user->plan->videos == 0)
			{{ __('tubelivery.plan_all_videos') }}
			@else
			{{ __('tubelivery.plan_videos', array('video' => $user->plan->videos)) }}<br/>
			@endif
		</p>
		<p>
			@if($user->plan->channels == 0)
			{{ __('tubelivery.plan_all_channels') }}<br/>
			@else
			{{ __('tubelivery.plan_channels', array('channel' => $user->plan->channels)) }}<br/>
			@endif
		</p>
	</div>
</div>
@endsection