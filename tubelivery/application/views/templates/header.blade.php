<body>
	<?
	$logged = Auth::check();
	$current_page = URI::segment(2);
	$login = '';
	$signup = '';
	$channels = '';
	$account = '';
	if ($current_page == 'login')
		$login = ' current_page_item';
	elseif ($current_page == 'signup')
		$signup = ' current_page_item';
	elseif ($current_page == 'channels')
		$channels = ' current_page_item';
	elseif ($current_page == 'account')
		$account = ' current_page_item';
	?>
	@if ($current_page == 'login' OR $current_page == 'signup')
	<div id="fb-root"></div>
	<script>
		var connected = false;
		window.fbAsyncInit = function() {
			// init the FB JS SDK
			FB.init({
				appId      : '545748532116979', // App ID from the App Dashboard
				channelUrl : '//tubelivery.com/fb_channel', // Channel File for x-domain communication
				status     : true, // check the login status upon init?
				cookie     : true, // set sessions cookies to allow your server to access the session?
				xfbml      : true  // parse XFBML tags on this page?
			});

			// Additional initialization code such as adding Event Listeners goes here
			signVerify();
		};

		function login() {
			if (connected)
				facebookLogIn();
			else
			{
				console.log('not connected');
				FB.login(function(response) {
					if (response.authResponse) {
						console.log('connected');
						facebookLogIn();
					} else {
						console.log('cancelled');
					}
				},
				{scope: 'email'}
			);
			}
		}

		function facebookLogIn() {
			FB.api('/me', function(response) {
				jQuery.post('facebook', response, function(data){
					if (data == 'login') {
						window.location.href = 'channels';
					}
				});
			});
		}

		function signVerify(){

			FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					for(i in response)
						console.log('response \''+i+'\': ' + response[i]);

					for(i in response.authResponse)
						console.log('authResponse \''+i+'\': ' + response.authResponse[i]);

					var signedRequest = response.authResponse.signedRequest;
					console.log('calling - facebook/sign/' + signedRequest);
					jQuery.get('facebook/sign/' + signedRequest, '', function(data){
						if (data == 'signed')
						{
							console.log('Loged in');
							connected = true;
						}
					});
					//					facebookLogIn();
				} else if (response.status === 'not_authorized') {
					console.log('not authorized');
				} else {
					console.log('not logged in');
				}
			});
		}

		// Load the SDK's source Asynchronously
		// Note that the debug version is being actively developed and might
		// contain some type checks that are overly strict.
		// Please report such bugs using the bugs tool.
		(function(d, debug){
			var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
			if (d.getElementById(id)) {return;}
			js = d.createElement('script'); js.id = id; js.async = true;
			js.src = "//connect.facebook.net/{{ __('tubelivery.locale') }}/all" + (debug ? "/debug" : "") + ".js";
			ref.parentNode.insertBefore(js, ref);
		}(document, /*debug*/ false));
	</script>
	@endif
	<div class="navbar navbar navbar-fixed-top">
		<div class="navbar-inner top-header">
			<div class="container">
				@if($logged)
				<a href="http://app.tubelivery.com/{{ __('tubelivery.lang') }}/channels/" title="Tubelivery">
					@else
					<a href="http://tubelivery.com/{{ __('tubelivery.lang') }}/" title="Tubelivery">
						@endif
						<img src="http://tubelivery.com/wp-content/uploads/2013/02/logo-170x75.png" alt="Tubelivery-logo" id="logo">
					</a>
					<div id="social-app"><a href="http://www.twitter.com/tubelivery" target="_blank">
							<img src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/images/social/default/twitter.png" alt="Twitter">
						</a>
						<a href="http://www.facebook.com/tubelivery" target="_blank">
							<img src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/images/social/default/facebook.png" alt="Facebook">
						</a>
						<a href="mailto:tubelivery@tubelivery.com">
							<img src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/images/social/default/email.png" alt="Email">
						</a>
					</div>
					<div class="span1 offset8 header-widget">
						<div id="qtranslate-3" class="widget-container widget_qtranslate">
							<ul class="qtrans_language_chooser" id="qtranslate-3-chooser">
								<?
								$language_names = Config::get('application.language_names');
								foreach (Config::get('application.languages') as $key => $lang)
								{
									?>
									<li>
										{{ HTML::link(URL::to_language($lang), '', array('class' => "qtrans_flag_$lang qtrans_flag")) }}
									</li>
									<?
								}
								?>
							</ul>
							<div class="qtrans_widget_end"></div>
						</div>
					</div>
			</div>
		</div>
		<div class="navbar-inner bottom-header">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<div class="nav-collapse collapse">
					<div class="menu-main-menu-container">
						<ul id="menu-main-menu" class="nav">
							@if($logged)
							<li class="menu-item menu-item-type-post_type menu-item-object-page{{ $channels }}">
								{{ HTML::link('channels', __('tubelivery.menu_channels')) }}
							</li>
							<li class="menu-item menu-item-type-post_type menu-item-object-page{{ $account }}">
								{{ HTML::link('account', __('tubelivery.menu_account')) }}
							</li>
							@else
							<li class="menu-item menu-item-type-post_type menu-item-object-page">
								{{ HTML::link('http://tubelivery.com/' . __('tubelivery.lang') . '/tubelivery/', 'Tubelivery') }}
							</li>
							<li class="menu-item menu-item-type-post_type menu-item-object-page">
								{{ HTML::link('http://tubelivery.com/' . __('tubelivery.lang') . '/podcast/', 'Podcast') }}
							</li>
							@endif
						</ul>
					</div>
					<div class="menu-login-menu-container">
						<ul id="menu-login-menu" class="nav pull-right">
							@if($logged)
							<!--						<li class="menu-item menu-item-type-custom menu-item-object-custom">-->
							<!--							{{ HTML::link('signup', __('tubelivery.menu_support')) }}-->
							<!--						</li>-->
							<li class="menu-item menu-item-type-custom menu-item-object-custom">
								{{ HTML::link('logout', __('tubelivery.menu_logout')) }}
							</li>
							@else
							<li class="menu-item menu-item-type-custom menu-item-object-custom{{ $login }}">
								{{ HTML::link('login', __('tubelivery.menu_login')) }}
							</li>
							<li class="menu-item menu-item-type-custom menu-item-object-custom{{ $signup }}">
								{{ HTML::link('signup', __('tubelivery.menu_signup')) }}
							</li>
							@endif
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div id="container" class="row-fluid">