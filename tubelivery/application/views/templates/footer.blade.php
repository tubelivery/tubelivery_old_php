</div>
</div>
<footer class="footer navbar-fixed-bottom">
	{{ Mixpanel::badge('', array('class' => 'mixpanel-badge')) }}
	<?
	$logged = Auth::check();
	?>
	@if(!$logged)
	<ul>
		<li class="page_item page-item-63"><a href="http://tubelivery.com/{{ __('tubelivery.lang') }}/versions/">{{ __('tubelivery.footer_version') }}</a></li>
		<li class="page_item page-item-61"><a href="http://tubelivery.com/{{ __('tubelivery.lang') }}/terms/">{{ __('tubelivery.footer_terms') }}</a></li>
		<li class="page_item page-item-66"><a href="http://tubelivery.com/{{ __('tubelivery.lang') }}/about/">{{ __('tubelivery.footer_about') }}</a></li>
	</ul>
	@endif
	<div id="copyright">
		{{ __('tubelivery.copyright') }}
	</div>
</footer>
<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/ultimate-tables/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>