<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="ie ie6 lte9 lte8 lte7" lang="{{ __('tubelivery.lang') }}">
<![endif]-->
<!--[if IE 7]>
<html class="ie ie7 lte9 lte8 lte7" lang="{{ __('tubelivery.lang') }}">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8 lte9 lte8" lang="{{ __('tubelivery.lang') }}">
<![endif]-->
<!--[if IE 9]>
<html class="ie ie9" lang="{{ __('tubelivery.lang') }}">
<![endif]-->
<!--[if gt IE 9]>
<html lang="{{ __('tubelivery.lang') }}">
<![endif]-->
<!--[if !IE]><!-->
<html lang="{{ __('tubelivery.lang') }}">
	<!--<![endif]-->
    <head>
		<meta charset="UTF-8">
		<title>Tubelivery | {{ __('tubelivery.' . URI::current()) }}</title>

		<meta name="apple-mobile-web-app-status-bar-style" content="translucent">
		<script type="text/javascript">
			if (window.screen.height==568)// iPhone 4"
				document.querySelector("meta[name=viewport]").content="width=320.1";
			else
				document.querySelector("meta[name=viewport]").content="width=device-width";

		</script>
		<script type="text/javascript">
			var addToHomeConfig = {
				animationIn: 'bubble',
				animationOut: 'drop',
				lifespan:10000,
				expire:2,
				touchIcon:true
			};
		</script>
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no;">
		<!--<meta name="apple-mobile-web-app-capable" content="yes">-->

		<link href="http://app.tubelivery.com/img/Default-568h_{{ __('tubelivery.lang') }}.png" rel="apple-touch-startup-image" media="(device-height: 568px)">
		<link href="http://app.tubelivery.com/img/Default_{{ __('tubelivery.lang') }}.png" rel="apple-touch-startup-image" sizes="640x920" media="(device-height: 480px)">

		<meta name="apple-mobile-web-app-title" content="Tubelivery">

		<link rel="apple-touch-icon-precomposed" href="http://tubelivery.com/wp-content/uploads/2013/02/favicon-200.png">
		<link rel="shortcut icon" href="http://tubelivery.com/wp-content/uploads/2013/02/favicon.png" type="image/x-icon">

		<link rel="profile" href="http://gmpg.org/xfn/11">
		<!--<link rel="pingback" href="http://tubelivery.com/en/xmlrpc.php">-->

		<!-- IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
			<script src="http://tubelivery.com/wp-content/themes/cyberchimps/inc/js/html5.js" type="text/javascript"></script>
		<![endif]-->

		<meta name="robots" content="noindex,nofollow">
		<link rel="alternate" type="application/rss+xml" title="Tubelivery » Feed" href="http://tubelivery.com/en/feed/">
		<link rel="alternate" type="application/rss+xml" title="Tubelivery » Comments Feed" href="http://tubelivery.com/en/comments/feed/">
		<link rel="stylesheet" id="admin-bar-css" href="http://tubelivery.com/wp-includes/css/admin-bar.min.css?ver=3.5.1" type="text/css" media="all">
		<link rel="stylesheet" id="ultimate-tables-style-css" href="http://tubelivery.com/wp-content/plugins/ultimate-tables/css/ultimate-tables.css?ver=3.5.1" type="text/css" media="all">
		<link rel="stylesheet" id="jquery.tipTip-css" href="http://tubelivery.com/wp-content/plugins/wp-tooltip/js/tipTip.css?ver=1.3" type="text/css" media="all">
		<link rel="stylesheet" id="wp-tooltip-css" href="http://tubelivery.com/wp-content/plugins/wp-tooltip/wp-tooltip.css?ver=1.0.0" type="text/css" media="all">
		<link rel="stylesheet" id="synved-shortcode-jquery-ui-css" href="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/jqueryUI/css/snvdshc/jquery-ui-1.9.2.custom.min.css?ver=1.9.2" type="text/css" media="all">
		<link rel="stylesheet" id="synved-shortcode-layout-css" href="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/style/layout.css?ver=1.0" type="text/css" media="all">
		<link rel="stylesheet" id="synved-shortcode-jquery-ui-custom-css" href="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/style/jquery-ui.css?ver=1.0" type="text/css" media="all">
		<link rel="stylesheet" id="bootstrap-style-css" href="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/bootstrap/css/bootstrap.min.css?ver=2.0.4" type="text/css" media="all">
		<link rel="stylesheet" id="bootstrap-responsive-style-css" href="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/bootstrap/css/bootstrap-responsive.min.css?ver=2.0.4" type="text/css" media="all">
		<link rel="stylesheet" id="core-style-css" href="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/css/core.css?ver=1.0" type="text/css" media="all">
		<link rel="stylesheet" id="style-css" href="http://tubelivery.com/wp-content/themes/cyberchimps/style.css?ver=1.0" type="text/css" media="all">
		<link rel="stylesheet" id="elements_style-css" href="http://tubelivery.com/wp-content/themes/cyberchimps/elements/lib/css/elements.css?ver=3.5.1" type="text/css" media="all">
		<link rel="stylesheet" id="social-buttons" href="http://app.tubelivery.com/css/auth-buttons.css" type="text/css" media="all">
		<link rel="stylesheet" id="social-buttons" href="http://app.tubelivery.com/css/add2home.css" type="text/css" media="all">
		<!--<script type="text/javascript" src="http://app.tubelivery.com/js/add2home.js"></script>-->
		<!--<script type="text/javascript" src="http://app.tubelivery.com/js/apple-webapp-link.js"></script>-->
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/jquery.js?ver=1.8.3"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/wp-tooltip/js/jquery.tipTip.minified.js?ver=1.3"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/wp-tooltip/js/wp-tooltip.js?ver=1.0.0"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery.ba-bbq.min.js?ver=1.2.1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery.scrolltab.js?ver=1.0"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.core.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.widget.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.tabs.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.accordion.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.button.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/script/jquery-unselectable.js?ver=1.0.0"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.mouse.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/jquery/ui/jquery.ui.slider.min.js?ver=1.9.2"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/script/base.js?ver=1.0"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/plugins/synved-shortcodes/synved-shortcode/script/custom.js?ver=1.0"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/jquery.slimbox.js?ver=1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/jquery.jcarousel.min.js?ver=1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/custom.js?ver=1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/jquery.mobile.custom.min.js?ver=3.5.1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/swipe-call.js?ver=3.5.1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-includes/js/comment-reply.min.js?ver=3.5.1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/cyberchimps/lib/js/video.js?ver=3.5.1"></script>
		<script type="text/javascript" src="http://tubelivery.com/wp-content/themes/cyberchimps/elements/lib/js/elements.js?ver=3.5.1"></script>
		<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://tubelivery.com/xmlrpc.php?rsd">
		<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://tubelivery.com/wp-includes/wlwmanifest.xml">
		<meta name="generator" content="Laravel 3">

		<meta http-equiv="Content-Language" content="{{ __('tubelivery.lang') }}">
		<style type="text/css" media="screen">
			.qtrans_flag span { display:none }
			.qtrans_flag { height:12px; width:18px; display:block }
			.qtrans_flag_and_text { padding-left:20px }
			.qtrans_flag_en { background:url(http://tubelivery.com/wp-content/plugins/qtranslate/flags/gb.png) no-repeat }
			.qtrans_flag_pt { background:url(http://tubelivery.com/wp-content/plugins/qtranslate/flags/br.png) no-repeat }
		</style>
		<link hreflang="{{ __('tubelivery.other_lang') }}" href="http://tubelivery.com/{{ __('tubelivery.other_lang') }}/" rel="alternate">
		<script type="text/javascript">

			var _gaq = _gaq || [];

			var pluginUrl = '//www.google-analytics.com/plugins/ga/inpage_linkid.js';
			_gaq.push(['_require', 'inpage_linkid', pluginUrl]);
			_gaq.push(['_setAccount', 'UA-37706820-1']);
			_gaq.push(['_setDomainName', 'tubelivery.com']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

		</script>
		<? include Bundle::path('mixpanel') . 'lib/include.php'; ?>
	</head>
