@layout('templates.base')
@section('content')
<div class="signup span4 offset4">
    {{ Form::open('signup', 'POST', array('class' => 'form-auth')) }}
	@if ($errors->has())
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		@foreach($errors->all() as $message)
		<p>{{ $message }}</p>
		@endforeach
	</div>
	@endif
	<h2 class="form-signin-heading">{{ __('tubelivery.signup') }}</h2>
	<div class="enter-form-center">
		<a class="btn-auth btn-facebook large enter-form-center" onclick="login()" href="javascript:void(0);">
			{{ __('tubelivery.signup_facebook') }}
		</a>
	</div>
	<p class="enter-with-email enter-form-center">{{ __('tubelivery.or_email') }}</p>
	{{ Form::text('name', Input::old('name'), array('class'=> 'input-block-level', 'autofocus' => 'autofocus', 'placeholder' => __('tubelivery.name'))) }}
	{{ Form::text('email', Input::old('email'), array('class'=> 'input-block-level', 'placeholder' => __('tubelivery.email'))) }}
	{{ Form::password('password', array('class'=> 'input-block-level', 'placeholder' => __('tubelivery.password'))) }}
	{{ Form::submit(__('tubelivery.menu_signup'), array('class' => 'btn btn-large')) }}
	{{ Form::token() }}
    {{ Form::close() }}
</div>
@endsection