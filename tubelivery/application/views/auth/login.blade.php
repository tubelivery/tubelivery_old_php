@layout('templates.base')
@section('content')
<div class="login span4 offset4">
    {{ Form::open('login', 'POST', array('class' => 'form-auth')) }}
	@if (Session::has('login_errors'))
	<div class="alert alert-error">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		<p>{{ __('tubelivery.login_error') }}</p>
	</div>
	@endif
	<h2 class="form-signin-heading">{{ __('tubelivery.login') }}</h2>
	<div class="enter-form-center">
		<a class="btn-auth btn-facebook large" onclick="login()" href="javascript:void(0);">
			{{ __('tubelivery.login_facebook') }}
		</a>
	</div>
	<p class="enter-form-center">{{ __('tubelivery.or_email') }}</p>
	{{ Form::text('email', Input::old('email'), array('class'=> 'input-block-level', 'autofocus' => 'autofocus', 'placeholder' => __('tubelivery.email'))) }}
	{{ Form::password('password', array('class'=> 'input-block-level', 'placeholder' => __('tubelivery.password'))) }}
	<label class="checkbox">
		{{ Form::checkbox('remember', 'true', Input::old('remember')) }}
		{{ __('tubelivery.remember_me') }}
	</label>
	{{ Form::submit(__('tubelivery.menu_login'), array('class' => 'btn btn-large')) }}
	{{ Form::token() }}
	{{ Form::close() }}
</div>
@endsection