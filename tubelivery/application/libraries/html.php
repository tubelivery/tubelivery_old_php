<?php

class Html extends \Laravel\HTML {

	public static function link($url, $title = null, $attributes = array(), $https = null)
	{
		$url = URL::to($url, $https);

		if (is_null($title))
			$title = $url;

		if (isset($attributes['html']) && $attributes['html'])
		{
			$title = $title;
			unset($attributes['html']);
		}
		else
			$title = static::entities($title);

		return '<a href="' . $url . '"' . static::attributes($attributes) . '>' . $title . '</a>';
	}

}