<?php

class Youtube {

	/**
	 * U
	 * Get all new videos from a channel and insert in the videos table
	 * @param Channel $channel Channel object that has the videos
	 * @return boolean TRUE if videos were inserted, false otherwise
	 */
	public static function insert_channel_videos($channel)
	{
		Log::DEBUG('insert_channel_videos');
		// Check how many videos are new
		$new_videos_count = $channel->videos - count($channel->videos()->results());

		// All videos have been downloaded
		if ($new_videos_count == 0)
		{
			return FALSE;
		}

		$limit = 50;
		$yt_username = $channel->yt_username;

		// Set the number off feed pages needed to get all videos. It's limited
		// to 20 feed pages as YouTube only allow 1000 videos in it's API
		$feed_pages_count = ceil($new_videos_count / $limit) > 20 ? 20 : ceil($new_videos_count / $limit);
		$j = 0;
		$videos = array();
		for ($i = 0; $i < $feed_pages_count; $i++)
		{
			$start_index = ($i * $limit) + 1;

			//Retrieve the videos
			$user_uploads = json_decode(file_get_contents("http://gdata.youtube.com/feeds/api/users/$yt_username/uploads?alt=json&orderby=published&max-results=$limit&start-index=$start_index"));

			foreach ($user_uploads->feed->entry as $video)
			{
				// Verify if all new videos has been inserted
				if ($j >= $new_videos_count)
				{
					break;
				}

				// Get the video URL
				$url = $video->id->{'$t'};

				// Get the YouTube video ID
				$array = explode("/", $url);
				$array_end = count($array) - 1;
				$yt_video_id[$j] = $array[$array_end];

				// Create the videos to be inserted array
				$videos[$yt_video_id[$j]] = array(
					'channel_id' => $channel->get_key(),
					'youtube_video_id' => $yt_video_id[$j],
					'url' => $url,
					'label' => $video->title->{'$t'},
					'description' => $video->content->{'$t'},
					'duration' => $video->{'media$group'}->{'yt$duration'}->seconds,
					'pub_date' => $video->published->{'$t'},
					'author' => $channel->label,
					'icon_url' => $video->{'media$group'}->{'media$thumbnail'}[0]->url,
					'created_at' => new \DateTime,
					'updated_at' => new \DateTime,
				);
				$j++;
			}
		}

		/**
		 * Array of videos already on DB
		 */
		$already_inserted = Video::select('youtube_video_id')
				->where('channel_id', '=', $channel->get_key())
				->where_in('youtube_video_id', $yt_video_id)
				->get();

		Log::debug("already_inserted has " . count($already_inserted) . " items");

		//Check if some video has already been inserted
		if (count($already_inserted) > 0)
		{
			Log::debug('Skiping ' . count($already_inserted) . ' videos (already inserted)');
			$j -= count($already_inserted);

			/**
			 * Unset the videos that have been inserted
			 */
			foreach ($already_inserted as $video)
			{
//				Log::debug('Skiped video with youtube_video_id ' . $video->youtube_video_id);
				unset($videos[$video->youtube_video_id]);
			}
		}

		// Insert the new videos in the DB
		// array_values eliminates the unseted videos from the array
		if (count(array_values($videos)))
		{
			Video::insert(array_values($videos));
			Log::debug("$j new videos were inserted in 'videos' for 'channel' '$channel->label'");
			return $videos;
		}
		return FALSE;
	}

	/**
	 * U
	 * Add the paths for a channel and a quality to the DB queue
	 * @param Video $videos Array of Video Objects
	 * @param Quality $quality Quality Object
	 * @return Array Array of Path Objects
	 */
	public static function add_videos_to_queue($channel, $quality)
	{
		Log::add_videos_to_queue('add_videos_to_queue');

		$videos = Video::where('channel_id', '=', $channel->get_key())
				->order_by('pub_date', 'desc')
				->get();

		$count = count($videos);
		Log::add_videos_to_queue($count);
		$video_ids = array();

		foreach ($videos as $video)
			$video_ids[] = $video->get_key();

		$already_inserted_paths = Path::get_inserted_paths($quality, $video_ids);

		foreach ($already_inserted_paths as $path)
		{
			$found = array_search($path->video_id, $video_ids);
			unset($videos[$found]);
			Log::debug('found');
		}
		$i = 1;
		$attributes = array();

		foreach ($videos as $video)
		{
			$attributes[] = array(
				'video_id' => $video->id,
				'quality_id' => $quality->id,
				'status' => Path::get_status($i),
				'created_at' => new \DateTime,
				'updated_at' => new \DateTime,
			);

			$i++;
		}
		Path::insert($attributes);
	}

	/**
	 * U
	 * Verify if YouTube is unreachable
	 * @return boolean TRUE if unreachable, FALSE if reachable
	 */
	public static function is_unreachable()
	{
		if (!fsockopen('www.youtube.com', 80, $errno, $errstr, 2))
			return TRUE;
		else
			return FALSE;
	}

	/**
	 * U
	 * If user inserted the channel URL instead of the name, remove the URL and
	 * return only the yt_username
	 * @param String $url The channel URL or the channel name
	 * @return String The channel name
	 */
	public static function clean_username($url)
	{
		// Remove the URL to get only the $yt_username
		$array = explode("/", $url);
		$limit = count($array) - 1;
		return Str::lower($array[$limit]);
	}

	/**
	 * U
	 * Reach YouTube and verify if a channel exists
	 * @param String $yt_username The YouTube channel name
	 * @return boolean TRUE if channel doesn't exist, FALSE if exists
	 */
	public static function channel_does_not_exist($yt_username)
	{
		$yt_user_feed = "http://gdata.youtube.com/feeds/api/users/$yt_username?alt=json";
		$headers = get_headers($yt_user_feed);

		// Verify if channel exists
		if (strpos($headers[0], '200') === FALSE)
			return TRUE;
		else
			return FALSE;
	}

	/**
	 * U
	 * Get all needed attributes from a channel
	 * @param String $yt_username The YouTube user name
	 * @return Array Array with these attributes:
	 * <ul>
	 * <li>label - The channel label</li>
	 * <li>yt_username - The YouTube user name</li>
	 * <li>yt_id - The channel ID</li>
	 * <li>icon_url - The channel icon URL</li>
	 * <li>description - The channel description</li>
	 * <li>videos - The number of videos of the channel</li>
	 * <li>subscribers - The number of subscribers of the channel</li>
	 * <li>exibitions - The number of exibitions of the channel</li>
	 * </ul>
	 */
	public static function get_channel_attributes($yt_username)
	{
		$channel_data = json_decode(file_get_contents("http://gdata.youtube.com/feeds/api/users/$yt_username?alt=json"));

		// Get the YouTube channel ID
		$array = explode("/", $channel_data->entry->id->{'$t'});
		$limit = count($array) - 1;
		$yt_id = $array[$limit];

		$attributes = array(
			'label' => $channel_data->entry->title->{'$t'},
			'yt_username' => $yt_username,
			'yt_id' => $yt_id,
			'icon_url' => $channel_data->entry->{'media$thumbnail'}->url,
			'description' => $channel_data->entry->content->{'$t'},
			'videos' => $channel_data->entry->{'gd$feedLink'}[4]->countHint,
			'subscribers' => $channel_data->entry->{'yt$statistics'}->subscriberCount,
			'exibitions' => $channel_data->entry->{'yt$statistics'}->viewCount,
		);
		return $attributes;
	}

	/**
	 * U
	 * RUNS IN BACKGROUND!<br/>
	 * Get the channel's videos and insert them in the DB. Runs in background
	 * using PHP exec of an artisian task.
	 * This function sets the status columns in the channels table with one
	 * of these values:
	 * <ul>
	 * <li>0 - no execution made OR execution had an error</li>
	 * <li>1 - execution is running</li>
	 * <li>2 - execution finished with success</li>
	 * </ul>
	 * After that, it inserts all channel's videos to the download queue.
	 * @param Channel $channel The channel object
	 * @param int $quality_id The quality ID
	 */
	public static function save_channel_videos_in_bg($channel, $quality_id)
	{
		//Insert the channel videos in the DB
		$enviroment = Request::env();
		$channel_key = $channel->get_key();
		$command = "php artisan youtube_dl $channel_key $quality_id --env=$enviroment > /dev/null &";
		Log::debug("exec($command)");
		exec($command);
	}

}