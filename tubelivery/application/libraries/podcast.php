<?php

class Podcast {

	public static function create_podcast_unique_hash($user_id, $channel_id)
	{
		$size = 74 - strlen($user_id . $channel_id . ($user_id + $channel_id));

		$hash = Str::random(6) .
				$user_id .
				$channel_id .
				Str::random($size) .
				($user_id + $channel_id);

		return $hash;
	}

	/**
	 * Get a feed by it's podcast hash
	 * @param string $podcast_hash The podcast hash
	 * @return boolean|string
	 */
	public static function feed($podcast_hash)
	{
		$channel_and_quality_ids = static::get_channel_and_quality_id_by_podcast_hash($podcast_hash);

		if ($channel_and_quality_ids === FALSE)
		{
			Log::info("Podcast with hash '$podcast_hash' not found");
			return 'not found';
		}

		$channel = Channel::find($channel_and_quality_ids->channel_id);
		$quality = Quality::find($channel_and_quality_ids->quality_id);

		//Update channel attributes and videos
		$channel = Channel::update_channel($channel);

		//Search and download new videos in BG
		Youtube::save_channel_videos_in_bg($channel, $quality->get_key());

		$user = User::get_by_podcast_hash($podcast_hash);

		return Path::get_downloaded_videos($channel, $quality, $user);
	}

	/**
	 * Get a channel and the videos qualities by a podcast hash
	 * @param string $podcast_hash The podcast hash
	 * @return array|boolean <b>Array</b> with 'channel_id' and 'quality_id' if found or
	 * <b>FALSE</b> if channel is not found
	 */
	private static function get_channel_and_quality_id_by_podcast_hash($podcast_hash)
	{
		$result_array = DB::table('channel_user_quality')
				->where('podcast_hash', '=', $podcast_hash)
				->get(array('channel_id', 'quality_id'));

		if (count($result_array) > 0)
			return $result_array[0];
		else
			return FALSE;
	}

	public static function xml_scape($string)
	{
		return str_replace(array("&", "<", ">", "\"", "'"), array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"), $string);
	}

}