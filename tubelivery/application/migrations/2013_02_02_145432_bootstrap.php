<?php

class Bootstrap {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
		$this->users_table();
		$this->plans_table();
		$this->channels_table();
		$this->channel_user_quality_table();
		$this->qualities_table();
		$this->quality_plan_table();
		$this->videos_table();
		$this->paths_table();
		$this->quality_video_table();
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('plans');
		Schema::drop('channels');
		Schema::drop('channel_user_quality');
		Schema::drop('qualities');
		Schema::drop('quality_plan');
		Schema::drop('videos');
		Schema::drop('paths');
		Schema::drop('quality_video');
	}

	private function users_table()
	{
		// Create
		Schema::create('users', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();
					$table->string('name', 255);
					$table->string('email', 255)->unique();
					$table->string('password', 64);
//					$table->string('password_reset_hash');
//					$table->string('temp_password');
//					$table->string('remember_me');
//					$table->string('activation_hash');
//					$table->string('ip_address');
//					$table->string('status');
//					$table->string('activated');
//					$table->text('permissions');
					$table->timestamp('last_login');
					$table->integer('plan_id')->unsigned()->default(1);

					// created_at & updated_at
					$table->timestamps();

					// FKs
					$table->foreign('plan_id')->references('id')->on('plans');
				});
		// Insert
		DB::table('users')->insert(array(
			'name' => 'Eduardo Russo',
			'email' => 'russoedu@gmail.com',
			'password' => Hash::make('russoedu'),
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime,
			'plan_id' => 5,
		));
		DB::table('users')->insert(array(
			'name' => 'Vivian Nascimento de Carvalho',
			'email' => 'vicanaca@gmail.com',
			'password' => Hash::make('vicanaca'),
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime,
			'plan_id' => 5,
		));
	}

	private function plans_table()
	{
		// Create
		Schema::create('plans', function ($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->string('label', 32);
					$table->integer('videos');
					$table->decimal('price', 4, 2);
					$table->integer('channels');


					// created_at & updated_at
					$table->timestamps();
				});

		// Insert
		DB::table('plans')->insert(array(
			'label' => 'Free',
			'videos' => 10,
			'price' => 0,
			'channels' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('plans')->insert(array(
			'label' => 'Basic',
			'videos' => 25,
			'price' => 1.00,
			'channels' => 5,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('plans')->insert(array(
			'label' => 'Premium',
			'videos' => 0,
			'price' => 5.00,
			'channels' => 10,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('plans')->insert(array(
			'label' => 'Professional',
			'videos' => 0,
			'price' => 15.00,
			'channels' => 0,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('plans')->insert(array(
			'label' => 'Beta',
			'videos' => 50,
			'price' => 0.00,
			'channels' => 0,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
	}

	private function channels_table()
	{
		// Create
		Schema::create('channels', function ($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->string('label', 255);
					$table->string('yt_username', 255);
					$table->string('yt_id');
					$table->string('icon_url', 255);
					$table->text('description');
					$table->integer('videos')->unsigned();
					$table->integer('subscribers')->unsigned();
					$table->integer('exibitions')->unsigned();

					// created_at & updated_at
					$table->timestamps();
				});
	}

	private function channel_user_quality_table()
	{
		// Create
		Schema::create('channel_user_quality', function ($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->integer('user_id')->unsigned();
					$table->integer('channel_id')->unsigned();
					$table->integer('quality_id')->unsigned();
					$table->string('podcast_hash');
					$table->boolean('videos_downloaded')->default(NULL);

					// created_at & updated_at
					$table->timestamps();

					// Index
					$table->index('podcast_hash');

					// FKs
					$table->foreign('user_id')->references('id')->on('users');
					$table->foreign('channel_id')->references('id')->on('channels');
					$table->foreign('quality_id')->references('id')->on('qualities');
				});
	}

	private function qualities_table()
	{
		// Create
		Schema::create('qualities', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id');

					$table->string('label', 16);
					$table->integer('youtube_quality_id');
					$table->string('youtube_dl_format');

					// created_at & updated_at
					$table->timestamps();
				});
		// Insert
		DB::table('qualities')->insert(array(
			'label' => 'SD (360p)',
			'youtube_quality_id' => 18,
			'youtube_dl_format' => '18	:	mp4	[360x640]',
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('qualities')->insert(array(
			'label' => 'HD (720p)',
			'youtube_quality_id' => 22,
			'youtube_dl_format' => '22	:	mp4	[720x1280]',
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('qualities')->insert(array(
			'label' => 'Full HD (1080p)',
			'youtube_quality_id' => 37,
			'youtube_dl_format' => '37	:	mp4	[1080x1920]',
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
	}

	private function quality_plan_table()
	{
		// Create
		Schema::create('quality_plan', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->integer('plan_id')->unsigned();
					$table->integer('quality_id')->unsigned();

					// created_at & updated_at
					$table->timestamps();

					// FKs
					$table->foreign('plan_id')->references('id')->on('plans');
					$table->foreign('quality_id')->references('id')->on('qualities');
				});
		// Insert
		DB::table('quality_plan')->insert(array(
			'plan_id' => 1,
			'quality_id' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 2,
			'quality_id' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 2,
			'quality_id' => 2,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 2,
			'quality_id' => 3,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 3,
			'quality_id' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 3,
			'quality_id' => 2,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 3,
			'quality_id' => 3,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 4,
			'quality_id' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 4,
			'quality_id' => 2,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 4,
			'quality_id' => 3,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 5,
			'quality_id' => 1,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 5,
			'quality_id' => 2,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
		DB::table('quality_plan')->insert(array(
			'plan_id' => 5,
			'quality_id' => 3,
			'created_at' => new \DateTime,
			'updated_at' => new \DateTime
		));
	}

	private function videos_table()
	{

		// Create
		Schema::create('videos', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->integer('channel_id')->unsigned();
					$table->string('youtube_video_id', 16);
					$table->string('url', 255);
					$table->text('label');
					$table->text('description');
					$table->string('duration', 8);
					$table->timestamp('pub_date');
					$table->string('author', 128);
					$table->string('icon_url', 255);


					// Index
					$table->index('youtube_video_id');

					// created_at & updated_at
					$table->timestamps();

					// FKs
					$table->foreign('channel_id')->references('id')->on('channels');
				});
	}

	private function paths_table()
	{
		// Create
		Schema::create('paths', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->integer('video_id')->unsigned();
					$table->integer('quality_id')->unsigned();
					$table->string('path', 255);
					$table->integer('file_size')->unsigned();

					// created_at & updated_at
					$table->timestamps();

					// FKs
					$table->foreign('video_id')->references('id')->on('videos');
					$table->foreign('quality_id')->references('id')->on('qualities');
				});
	}

	private function quality_video_table()
	{
		// Create
		Schema::create('quality_video', function($table)
				{
					// auto incremental id (PK)
					$table->increments('id')->unsigned();

					$table->integer('video_id')->unsigned();
					$table->integer('quality_id')->unsigned();

					// created_at & updated_at
					$table->timestamps();

					// FKs
					$table->foreign('video_id')->references('id')->on('videos');
					$table->foreign('quality_id')->references('id')->on('qualities');
				});
	}

}