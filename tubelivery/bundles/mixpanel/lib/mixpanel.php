<?php

/**
 * This is the class that implements all Mixpanel APIs
 * Learn more about Mixpanel in <http://mixpanel.com>
 *
 * @package 	mixpanel
 * @author 		Eduardo Russo
 * @copyright 	Eduardo Russo 2013
 * @license 	MIT License <http://www.opensource.org/licenses/mit>
 *
 */
class Mixpanel {

	/**
	 * Create the Mixpanel header
	 * @param Boolean $set_config TRUE to insert the configurations in the header.
	 * Default is FALSE
	 * @return String Formated Mixpanel JavaScript for header
	 */
	public static function init($set_config = FALSE)
	{
		$id = Config::get('mixpanel::config.mixpanel_id');

		$script_start = <<<HDR
(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src=("https:"===e.location.protocol?"https:":"http:")+'//cdn.mxpnl.com/libs/mixpanel-2.2.min.js';f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f);b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==
typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.increment people.append people.track_charge people.clear_charges people.delete_user".split(" ");for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,
e,d])};b.__SV=1.2}})(document,window.mixpanel||[]);

HDR;

		$init = "mixpanel.init(\"$id\");
			";

		$config = $set_config == TRUE ?
				static::set_config() :
				'';

		return $script_start . $init . $config;
	}

	/**
	 * Format the scripts for the Mixpanel JavaScript
	 * @param String $type The Mixpanel data type (ex: 'track', 'people.set')
	 * @param Mixed $params The Mixpanel data params as Array or String
	 * @return String The formated Mixpanel JavaScript
	 */
	private static function format_script($type, $params = array())
	{

		$script_start = "mixpanel.$type(";

		$script_end = ');';

		if (is_array($params) && count($params) == 0)
			return '';
		if (!is_array($params))
			return "$script_start\"$params\"$script_end";
		else
		{
			$values = '';
			foreach ($params as $name => $value)
			{
				if (is_numeric($name))
				{
					$values .= "\"$value\",
					";
				}
				else
				{
					if (!is_numeric($value))
					{
						$value = "\"$value\"";
					}

					$values .= "\"$name\": $value,
					";
				}
			}
			return $script_start . '{' . $values . '}' . $script_end;
		}
	}

	/**
	 * Set configuration options for the Javascript tracker. These values will
	 * be passed in when mixpanel.init() is called.
	 *
	 * Example:<br/>
	 * mixpanel.set_config({<br/>
	 * 			    "cross_subdomain_cookie": "false",<br/>
	 * 				"debug": "true"<br/>
	 * 			});<br/>
	 * @return String The formated config string with all non default values.
	 * Every default value will be ignored
	 */
	public static function set_config()
	{
		$cross_subdomain_cookie = Config::get('mixpanel::config.cross_subdomain_cookie');
		$cookie_name = Config::get('mixpanel::config.cookie_name');
		$store_google = Config::get('mixpanel::config.store_google');
		$save_referrer = Config::get('mixpanel::config.save_referrer');
		$test = Config::get('mixpanel::config.test');
		$verbose = Config::get('mixpanel::config.verbose');
		$track_pageview = Config::get('mixpanel::config.track_pageview');
		$debug = Config::get('mixpanel::config.debug');
		$track_links_timeout = Config::get('mixpanel::config.track_links_timeout');
		$cookie_expiration = Config::get('mixpanel::config.cookie_expiration');
		$upgrade = Config::get('mixpanel::config.upgrade');
		$loaded = Config::get('mixpanel::config.loaded');

		$params = array();

		if ($cross_subdomain_cookie === FALSE)
			$params['cross_subdomain_cookie'] = $cross_subdomain_cookie;
		if ($cookie_name !== '')
			$params['cookie_name'] = $cookie_name;
		if ($store_google === FALSE)
			$params['store_google'] = $store_google;
		if ($save_referrer === FALSE)
			$params['save_referrer'] = $save_referrer;
		if ($test === TRUE)
			$params['test'] = $test;
		if ($verbose === TRUE)
			$params['verbose'] = $verbose;
		if ($track_pageview === FALSE)
			$params['track_pageview'] = $track_pageview;
		if ($debug === TRUE)
			$params['debug'] = $debug;
		if ($track_links_timeout !== 300 && $track_links_timeout !== '')
			$params['track_links_timeout'] = $track_links_timeout;
		if ($cookie_expiration !== 365 && $cookie_expiration !== '')
			$params['cookie_expiration'] = $cookie_expiration;
		if ($upgrade === TRUE)
			$params['upgrade'] = $upgrade;
		if ($loaded !== '')
			$params['loaded'] = $loaded;

		return static::format_script('set_config', $params);
	}

	/**
	 * mixpanel.track
	 * https://mixpanel.com/docs/integration-libraries/javascript-full-api#track
	 *
	 * Track an event caused by a user. This is the most important Mixpanel
	 * function and is the one you will be using the most.
	 * @param Array $params
	 * <b>event_name:string</b> - required - The name of the event
	 * to track. This can be anything a user does - "button click", "user signup",
	 * "item purchased", etc. You can name your events anything you like.
	 *
	 * <b>properties:object</b> - optional - A set of properties to include with the
	 * event you're sending. These can describe the user who did the event or
	 * the event itself. Example values:{ user_type: "paid", button_id: 45 }
	 *
	 * <b>callback:function</b> - optional - A callback function to fire after
	 * the event is successfully tracked.
	 * JavaScript tags. Default is FALSE
	 * @return String Formated JavaScript
	 */
	public static function track($params)
	{
		return static::format_script('track', $params);
	}

	/**
	 * mixpanel.track_links
	 * https://mixpanel.com/docs/integration-libraries/javascript-full-api#track_links
	 *
	 * Track clicks on a set of links defined by a selector (a normal CSS
	 * selector, not jQuery style class or logical selectors). Note the CSS
	 * selector that you specify must be present on the page. Including references
	 * to absent selectors will cause an error. Also use of track links may
	 * interfere with existing onClick handlers for that element.
	 * @param Array $params
	 * <b>css_selector:string </b> - required - A valid CSS selector that matches
	 * the link(s) you want to track, preferably an ID. Example: "#header"
	 * <b>event_name:string</b> - required - The name of the event to send when
	 * a link is clicked. This can be anything you want, like "clicked header
	 * link" or "footer link click".
	 * <b>properties:object|function</b> - optional - This can be a set of
	 * properties, or a function that returns a set of properties after being
	 * passed a DOMElement. Example values: { user_type: "paid", button_id: 45 }
	 * or function(link) { return { link_type: $(link).attr('type') }; }.
	 * @return String Formated JavaScript
	 */
	public static function track_links($params)
	{
		return static::format_script('track_links', $params);
	}

	/**
	 * mixpanel.track_forms
	 * @param Array $params
	 * @return String Formated JavaScript
	 */
	public static function track_forms($params)
	{
		return static::format_script('track_forms', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function track_pageview($params)
	{
		return static::format_script('track_pageview', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function register($params)
	{
		return static::format_script('register', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function register_once($params)
	{
		return static::format_script('register_once', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function unregister($params)
	{
		return static::format_script('unregister', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function get_property($params)
	{
		return static::format_script('get_property', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function identify($params)
	{
		return static::format_script('identify', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function alias($params)
	{
		return static::format_script('alias', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function get_distinct_id($params)
	{
		return static::format_script('get_distinct_id', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function name_tag($params)
	{
		return static::format_script('name_tag', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function disable($params)
	{
		return static::format_script('disable', $params);
	}

	public static function people_set($params = array(), $created = NULL, $email = NULL, $first_name = NULL, $last_name = NULL, $name = NULL, $username = NULL)
	{
		if (!is_null($created))
			$params['$created'] = $created;
		if (!is_null($email))
			$params['$email'] = $email;
		if (!is_null($first_name))
			$params['$first_name'] = $first_name;
		if (!is_null($last_name))
			$params['$last_name'] = $last_name;
		if (!is_null($name))
			$params['$name'] = $name;
		if (!is_null($username))
			$params['$username'] = $username;

		return static::format_script('people.set', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function people_increment($params)
	{
		return static::format_script('people.increment', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function people_track_charge($params)
	{
		return static::format_script('people.track_charge', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function people_clear_charges($params)
	{
		return static::format_script('people.clear_charges', $params);
	}

	/**
	 *
	 * @param type $params
	 * @return String Formated JavaScript
	 */
	public static function people_delete_user($params)
	{
		return static::format_script('people.delete_user', $params);
	}

	/**
	 * This creates the Mixpanel Badge to gain 175,000 more data points
	 * Know more in <https://mixpanel.com/free/>
	 * @param Boolean $blue Use the blue version. Default is FALSE
	 * @param Array $params HTML params to use in the <a> tag
	 * @return String HTML code with the link and the image
	 */
	public static function badge($blue = FALSE, $params = array())
	{
		$color = $blue == TRUE ? 'blue' : 'light';

		$extra = '';

		foreach ($params as $key => $value)
		{
			$extra .= " $key=\"$value\"";
		}
		$badge = <<<BDG
<a href="https://mixpanel.com/f/partner"$extra>
	<img src="//cdn.mxpnl.com/site_media/images/partner/badge_$color.png" alt="Mobile Analytics" />
</a>
BDG;
		return $badge;
	}

}