<?php

/**
 * This is the include file to verify for passed Mixpanel Data. Insert an include
 * for this file in the header
 *
 * You can set the $set_config variable to insert the Mixpanel configurations
 *
 * @package 	mixpanel
 * @author 		Eduardo Russo
 * @copyright 	Eduardo Russo 2013
 * @license 	MIT License <http://www.opensource.org/licenses/mit>
 *
 */
echo '<script type="text/javascript">';

if (isset($set_config))
	echo Mixpanel::init(TRUE);
else
	echo Mixpanel::init();

if (Session::has('mixpanel_data'))
{
	foreach (Session::get('mixpanel_data') as $data)
		echo $data;
}
if (isset($mixpanel_data))
{
	foreach ($mixpanel_data as $data)
		echo $data;
}

echo "</script>";