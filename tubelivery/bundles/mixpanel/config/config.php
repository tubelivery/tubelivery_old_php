<?php

return array(
	/*
	 * mixpanel_id
	 * This is your unique identifier
	 */
	'mixpanel_id' => 'fbc95c5cb54adb5bb3582fd982d59522',
	/*
	 * cross_subdomain_cookie:boolean
	 * Default is true.
	 *
	 * If true, the Mixpanel cookie is set across all subdomains of your
	 * site, e.g. www.example.com, http://example.com.
	 */
	'cross_subdomain_cookie' => '',
	/*
	 * cookie_name:string
	 * If not set, we will generate a name.
	 *
	 * Manually set a name for your Mixpanel cookie.
	 */
	'cookie_name' => '',
	/*
	 * store_google:boolean
	 * Default is true.
	 *
	 * If true, we save Google adwords query params such as utm_source
	 * and utm_campaign as super properties.
	 */
	'store_google' => '',
	/*
	 * save_referrer:boolean
	 * Default is true.
	 *
	 * If true, we automatically save information about user's referrer
	 * the first time we see them.
	 */
	'save_referrer' => '',
	/*
	 * test:boolean
	 * Default is false.
	 *
	 * If true, data sent bypasses the normal queues. Only useful if
	 * Mixpanel data collection is backed up for some reason and you are
	 * just getting started. Test queue is rate-limited and should not
	 * be used in production.
	 */
	'test' => '',
	/*
	 * verbose:boolean
	 * Default is false.
	 *
	 * If true, the server will respond with JSON rather than just 0 or
	 * 1.  The JSON includes an error message if an error occurs, and can
	 * help debug integration issues.  It is not recommended to leave
	 * verbose enabled since it will slow down server responses.
	 */
	'verbose' => '',
	/*
	 * track_pageview:boolean
	 * Default is true.
	 *
	 * If true, we track a pageview event for Streams each time the
	 * library is loaded.
	 */
	'track_pageview' => '',
	/*
	 * debug:boolean
	 * Default is false.
	 *
	 * If true, we print debugging information to the Javascript console
	 * each time you make a track call.
	 */
	'debug' => '',
	/*
	 * track_links_timeout:integer
	 * Default is 300 ms.
	 *
	 * Timeout in milliseconds for track_links() and track_forms(). If
	 * the timeout expires, the initial link click or form submission is
	 * immediately continued.
	 */
	'track_links_timeout' => '',
	/*
	 * cookie_expiration:integer
	 * Default is 365.
	 *
	 * Number of days for Mixpanel super properties cookie to expire.
	 */
	'cookie_expiration' => '',
	/*
	 * upgrade:boolean
	 * Default is false.
	 *
	 * If true, we automatically convert super properties from the
	 * previous version of the library.
	 */
	'upgrade' => '',
	/*
	 * loaded:function
	 * If set, function is called just before the initial track_pageview
	 * call. This allows you to call identify() or name_tag() before
	 * sending that first pageview.
	 */
	'loaded' => '',
);
