#!/usr/bin/php -q
<?php
include('util.php');

// Allowed arguments & their defaults
$runmode = array(
	'no-daemon' => false,
	'help' => false,
	'write-initd' => false,
);

// Scan command line attributes for allowed arguments
foreach ($argv as $k => $arg)
{
	if (substr($arg, 0, 2) == '--' && isset($runmode[substr($arg, 2)]))
	{
		$runmode[substr($arg, 2)] = true;
	}
}

// Help mode. Shows allowed argumentents and quit directly
if ($runmode['help'] == true)
{
	echo 'Usage: ' . $argv[0] . ' [runmode]' . "\n";
	echo 'Available runmodes:' . "\n";
	foreach ($runmode as $runmod => $val)
	{
		echo ' --' . $runmod . "\n";
	}
	die();
}

// Setup
$options = array(
	'appName' => 'video-downloader',
	'appDir' => dirname(__FILE__),
	'appDescription' => 'Download queued videos',
	'authorName' => 'Eduardo Russo',
	'authorEmail' => 'tubelivery@tubelivery.com',
	'sysMaxExecutionTime' => '0',
	'sysMaxInputTime' => '0',
//    'sysMemoryLimit' => '1024M',
	'appRunAsGID' => 1000,
	'appRunAsUID' => 1000,
);

System_Daemon::setOptions($options);

// With the runmode --write-initd, this program can automatically write a
// system startup file called: 'init.d'
// This will make sure your daemon will be started on reboot
if ($runmode['write-initd'])
{
	System_Daemon::writeAutoRun(TRUE);
}

if (!$runmode['no-daemon'])
{
	// Spawn Daemon
	System_Daemon::start();
}

// Include Class
error_reporting(E_ALL);

$runningOkay = TRUE;
$loop = TRUE;

$status = INITIAL_STATUS;
$youtube_dl_update = 0;

System_Daemon::info('Started Daemon');

System_Daemon::setSigHandler(SIGHUP, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));
System_Daemon::setSigHandler(SIGTERM, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));

//System_Daemon::setSigHandler(SIGTERM, 'myHandler');

function myHandler($signal)
{
	System_Daemon::warning($signal);
	if ($signal === SIGTERM)
	{
		System_Daemon::warning('I received the termination signal. ' . $signal);
		// Execute some final code
		// and be sure to:
        System_Daemon::stop();
	}
	elseif ($signal === SIGHUP)
	{
		System_Daemon::warning('I received the start signal. ' . $signal);
		// Execute some final code
		// and be sure to:
        System_Daemon::stop();
	}
}

while (!System_Daemon::isDying() && $runningOkay && $loop)
{
	// What mode are we in?
	$mode = '"' . (System_Daemon::isInBackground() ? '' : 'non-' ) . 'daemon" mode';

	$videos = get_videos_with_status($status);
	$max_cicle = ceil(STATUS_CICLE_BREAK / $status);
	$max_videos = ceil(count($videos));

	System_Daemon::info("status $status: $max_videos new videos and $max_cicle videos per cicle");

	for ($i = 0; $i < $max_cicle && $i < $max_videos; $i++)
	{
		System_Daemon::info("video " . ($i + 1));

		//Download the video
		$video_info = download_video($videos[$i]['video_id'], $videos[$i]['quality'], $videos[$i]['channel']);

		if ($video_info !== FALSE)
		{
			System_Daemon::info("downloaded video " . ($i + 1) . " in " . $video_info['path']);
			update_video_state_to_downloaded($videos[$i]['id'], $video_info['path'], $video_info['size']);
		}
	}
	$status = $status == FINAL_STATUS ? INITIAL_STATUS : $status + STATUS_INCREMENT;

	if ($youtube_dl_update == YOUTUBE_DL_UPDATE_CICLE)
	{
		$update_status = update_youtube_dl();
		$youtube_dl_update = 0;
		System_Daemon::info('update_youtube_dl() returned with status ' . $update_status);
	}
	else
	{
		$youtube_dl_update++;
	}
	System_Daemon::info('sleeping ' . SLEEP_TIME . ' seconds');
	System_Daemon::iterate(SLEEP_TIME);
}

if (!$runningOkay)
{
	System_Daemon::err('error');
}
// Shut down the daemon nicely
// This is ignored if the class is actually running in the foreground
System_Daemon::stop();