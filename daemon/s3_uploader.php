#!/usr/bin/php -q
<?php
include('util.php');

// Allowed arguments & their defaults
$runmode = array(
	'no-daemon' => false,
	'help' => false,
	'write-initd' => false,
);

// Scan command line attributes for allowed arguments
foreach ($argv as $k => $arg)
{
	if (substr($arg, 0, 2) == '--' && isset($runmode[substr($arg, 2)]))
	{
		$runmode[substr($arg, 2)] = true;
	}
}

// Help mode. Shows allowed argumentents and quit directly
if ($runmode['help'] == true)
{
	echo 'Usage: ' . $argv[0] . ' [runmode]' . "\n";
	echo 'Available runmodes:' . "\n";
	foreach ($runmode as $runmod => $val)
	{
		echo ' --' . $runmod . "\n";
	}
	die();
}

// Setup
$options = array(
	'appName' => 's3-uploader',
	'appDir' => dirname(__FILE__),
	'appDescription' => 'Upload queued videos to S3',
	'authorName' => 'Eduardo Russo',
	'authorEmail' => 'tubelivery@tubelivery.com',
	'sysMaxExecutionTime' => '0',
	'sysMaxInputTime' => '0',
    'sysMemoryLimit' => '384M',
	'appRunAsGID' => 1000,
	'appRunAsUID' => 1000,
);

System_Daemon::setOptions($options);

// With the runmode --write-initd, this program can automatically write a
// system startup file called: 'init.d'
// This will make sure your daemon will be started on reboot
if ($runmode['write-initd'])
{
	System_Daemon::writeAutoRun(TRUE);
}

if (!$runmode['no-daemon'])
{
	// Spawn Daemon
	System_Daemon::start();
}

//error_reporting(E_ALL);

$runningOkay = TRUE;
$loop = TRUE;
$restart_cicle = 0;

System_Daemon::info('Started Daemon');

System_Daemon::setSigHandler(SIGHUP, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));
System_Daemon::setSigHandler(SIGTERM, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));

//System_Daemon::setSigHandler(SIGTERM, 'myHandler');

function myHandler($signal)
{
	System_Daemon::warning($signal);
	if ($signal === SIGTERM)
	{
		System_Daemon::warning('I received the termination signal. ' . $signal);
		// Execute some final code
		// and be sure to:
		System_Daemon::stop();
	}
	elseif ($signal === SIGHUP)
	{
		System_Daemon::warning('I received the start signal. ' . $signal);
		// Execute some final code
		// and be sure to:
		System_Daemon::stop();
	}
}

while (!System_Daemon::isDying() && $runningOkay && $loop)
{
	$videos = get_videos_with_status(DOWNLOADED_STATUS);
	$num_of_videos = count($videos);
	System_Daemon::info("found $num_of_videos videos to upload to S3");

	foreach ($videos as $i => $video)
	{

		// Upload a publicly accessible file. File size, file type, and md5 hash are automatically calculated by the SDK
		try
		{
			$start = (float) array_sum(explode(' ',microtime()));

			$save_path = "/tmp/$video[quality]/$video[video_id].mp4";
			$db_path = "$video[channel]/$video[quality]/$video[video_id].mp4";
			$bytes = number_format(filesize($save_path) / 1048576, 2) . ' MB';

			System_Daemon::info(($i + 1) . "/$num_of_videos - started upload of $video[channel] - $video[video_id] with $bytes");

			$s3 = Aws\S3\S3Client::factory(
							array(
								'key' => 'AKIAJCD77ZSTTII5KJFA',
								'secret' => '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',
								'region' => Aws\Common\Enum\Region::US_EAST_1
							)
			);


			$results = $s3->putObject(array(
				'Bucket' => 'media.tubelivery.com',
				'Key' => $db_path,
				'Body' => fopen($save_path, 'r'),
				'ACL' => Aws\S3\Enum\CannedAcl::PUBLIC_READ
					));

			//Delete the original file
			unlink($save_path);
			clearstatcache($save_path);

			//Change the video state to 0
			update_video_state_to_uploaded_to_S3($video['id']);

			$end = (float) array_sum(explode(' ',microtime()));
			$time = sprintf("%.4f", ($end - $start)) . " sec";

			System_Daemon::info("uploaded video " . ($i + 1) . " to $db_path in $time");
		}
		catch (Aws\S3\Exception\S3Exception $e)
		{
			System_Daemon::err("ERROR uploading $video[video_id].mp4 to S3");
			foreach($results as $key => $result)
			{
				System_Daemon::err("$key => $result");
			}
			$save_path = "/tmp/$video[quality]/$video[video_id].mp4";	
			clearstatcache($save_path);
			System_Daemon::err("ERROR: $e");
		}
	}
	if ($restart_cicle == RESTART_CICLE)
	{
		$restart_cicle = 0;
		System_Daemon::info('restarting s3-uploader');
		restart_service("s3-uploader");
	}
	else
	{
		$restart_cicle++;
	}

	System_Daemon::info('sleeping ' . UPLOAD_SLEEP_TIME . ' seconds');
	System_Daemon::iterate(UPLOAD_SLEEP_TIME);
}

if (!$runningOkay)
{
	System_Daemon::err('error');
}
// Shut down the daemon nicely
// This is ignored if the class is actually running in the foreground
System_Daemon::stop();