<?php

/**
 * Get Data Base Handler.
 * Manual @ http://www.php.net/manual/en/pdostatement.fetch.php
 * More info @ http://net.tutsplus.com/tutorials/php/why-you-should-be-using-phps-pdo-for-database-access/
 *
 * @return PDO Data Base Handler
 */
function DBH() {
	global $DBH;
	global $db_config;

	if ($DBH === null) {
		// throws PDOException on connection error
		$DBH = new PDO("mysql:host=$db_config[host];dbname=$db_config[dbname]", $db_config['user'], $db_config['pass'],
						array(PDO::ATTR_PERSISTENT => $db_config['persistent'], PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES '$db_config[encoding]'"));
		// ask PDO to throw exceptions for any error
		$DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	return $DBH;
}



/**
 * Create a Statement Handle and fetch a select
 * Manual @ http://www.php.net/manual/en/pdostatement.fetch.php
 *
 * @param $select - The select query
 * @param $fetch_style - PDO fetch style
 * fecth_styles info @ http://br2.php.net/manual/en/pdostatement.fetch.php
 * @param $params - an array or an array of arrays of Params to use in bindParam
 * Format: array(position in query, data value, data type)
 * data types info @ http://php.net/manual/en/pdo.constants.php
 * $params may be with a single array:
 * 		array(1, $id, PDO::PARAM_INT))
 * or many arrays inside the first:
 *		array(array(1, $id, PDO::PARAM_INT), array(2, $name, PDO::PARAM_STRING))
 * @param $single_result - boolean to return a single result or an array
 *
 * @return mixed - Query result
 */
function FETCH($select, $fetch_style, $single_result = FALSE){
	//Create DB Handle
	$DBH = DBH();

	// Create statement handle
	$sth = $DBH->prepare($select);

	//Execute statement handle
	$sth->execute();

	//Fetch a single result and return
	if($single_result)
		return $sth->fetch($fetch_style);
	//Fetch a array
	else
		return $sth->fetchAll($fetch_style);
}

/**
 * Execute an insert or update in the database.
 * @param $table - Table name.
 * @param $key_name - Primary key to update. NULL to a insert
 * @param $data - Column data array
 * @param $call_on_error function name that should called in case of an exception during the
 * execution of the statment, the function is expected to take one argument, the exception object
 * @return mixed An array containing the key inserted or updated on success, false on failure.
 */
function INSERT($table, $key_name, &$data, $call_on_error = null) {
    list($min_cols, $prefix, $suffix, $key_value) = isset($data[$key_name]) ?
		array(2, 'UPDATE', " WHERE `$key_name`=:$key_name", $data[$key_name]) :
		array(1, 'INSERT', '', null);
	if (count($data) < $min_cols) {
        echo "error";
		return false;
	}
	$set_clause = '';
	foreach ($data as $k => $v) {
		if ($k !== $key_name) {
			if ($flag_name = strstr($k, "__", true)) {
				if (strcmp($k, "{$flag_name}__mask") && isset($data["{$flag_name}__value"]))
					$set_clause .= ",`$flag_name`=:{$flag_name}__value | (`$flag_name` & :{$flag_name}__mask)";
			} else {
				$set_clause .= ",`$k`=:$k";
			}
		}
	}
    global $dbo_error_duplicated;
	$dbo_error_duplicated = false;
	$dbh = DBH();
	try {
		$sth = $dbh->prepare("$prefix $table SET " . substr($set_clause, 1) . $suffix);
		$res = $sth->execute($data);
	} catch (PDOException $e) {
		$dbo_error_duplicated = $sth->errorCode() === '23000';
		echo $e;
		if(isset($call_on_error)){
			call_user_func($call_on_error, $e);
		}
		$res = false;
	}
	if ($res) {
		if ($key_value === null && is_numeric($id = $dbh->lastInsertId())) {
			$key_value = $id;
		}
		$res = $key_value === null ? false : array($key_name => $key_value);
	}
	return $res;
}

function DELETE($table, $key_name, $key_value) {
    //Create DB Handle
    $DBH = DBH();
    $delete_statement = <<<DS
    DELETE
    FROM
        $table
    WHERE
        $key_name = "$key_value";
DS;

    $count = $DBH->exec($delete_statement);
    return $count;
}

?>