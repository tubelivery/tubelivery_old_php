<?php
//Fixed Params
$you_tube_feed = $video_config['feed_base'] . $_GET["channel"];

// <meta property="og:image" content="//i4.ytimg.com/i/WFsE0cjOc_iyHCYA_pVQ8w/1.jpg?v=a6f1c9">

$channel_image = get_channel_image($_GET['channel']);

$videos_path = $video_config['path'];
$url_path = get_base_url($_SERVER['REQUEST_URI']);
		
$limit = isset($_GET['limit']) ? $_GET['limit'] : $video_config['limit'];
$counter = 0;
$video_type = $video_config['type'];
$video_extension = $video_config['extension'];
$podcast_label;
		
//Feed optional parameters
if(isset($_GET["search"]))
	$you_tube_feed .= "&q=" . $_GET["search"];
		
$you_tube_feed .= "&max-results=" . $limit;

if(isset ($_GET["label"]))
	$podcast_label = str_replace(" ", "", $_GET["label"]);
else
	$podcast_label = str_replace(" ", "", $_GET["channel"]);

//http://en.wikipedia.org/wiki/YouTube#Quality_and_codecs
if (isset($_GET["quality"])){
	$quality = $_GET["quality"];
	if($quality == "sd"){
		$video_quality = 18;
		$podcast_label .= "SD";
	}
	else if($quality == "hd"){
		$video_quality = 22;
		$podcast_label .= "HD";
	}
	else if($quality == "fullhd"){
		$video_quality = 37;
		$podcast_label .= "FullHD";
	}
	
}
else{
	$video_quality = $video_config['quality'];
}
				
// Feeds and XMLs
$first_feed_xml = simplexml_load_file($you_tube_feed);

$search_data = $first_feed_xml->channel->children('http://a9.com/-/spec/opensearchrss/1.0/');
$video_count = $search_data->totalResults;
$feed_pages = ceil($video_count / $limit);

// echo("Quantidade de vídeos por XML: " . $limit . "\n");
// echo("Quantidade total de vídeos: " . $video_count . "\n");
// echo("Quantidade de páginas: " .$feed_pages . "\n");

// for ($i = 0; $i < $feed_pages; $i++){
// 	$start_index = ()$i * $limit) + 1;
// 	$youtube_feed_xml[$i] = simplexml_load_file("$you_tube_feed&start-index=$start_index");
// }
// // echo $youtube_feed_xml->channel->lastBuildDate;
// foreach ($youtube_feed_xml->channel->item as $item){
		
create_channel($podcast_label, $first_feed_xml->channel, $channel_image);

// $logger->info("YouTube feed:\n\n\n" . $you_tube_feed_xml->asXML() . "\n\n\n");

$logger->info("\n\nYouTube Feed: $you_tube_feed\n$limit videos is the LIMIT\n" . $video_count . " videos returned in all feeds \n");
?>
