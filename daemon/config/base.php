<?php

$configs = parse_ini_file("config.ini", true);
$db_config = $configs['db_config'];
$paths_config = $configs['paths'];

define('DOWNLOAD_SLEEP_TIME', 2);
define('UPLOAD_SLEEP_TIME', 2);

define('INITIAL_STATUS', 1);
define('FINAL_STATUS', 9);
define('DOWNLOADED_STATUS', 100);
define('STATUS_INCREMENT', 2);
define('STATUS_DOWNLOADED', 0);

define('STATUS_CICLE_BREAK', 550);
define('RESTART_CICLE', 5000);

//Libs and functions
require_once dirname(dirname(__FILE__)) . '/config/db_base.php';
require_once 'System/Daemon.php';
require_once dirname(dirname(__FILE__)) . '/lib/aws/vendor/autoload.php';

//use Aws\Common\Aws;
//use Aws\Common\Enum\Region;
//use Aws\S3\Enum\CannedAcl;
//use Aws\S3\Exception\S3Exception;
//use Aws\S3\S3Client;