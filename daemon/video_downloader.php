#!/usr/bin/php -q
<?php
include('util.php');

// Allowed arguments & their defaults
$runmode = array(
	'no-daemon' => false,
	'help' => false,
	'write-initd' => false,
);

// Scan command line attributes for allowed arguments
foreach ($argv as $k => $arg)
{
	if (substr($arg, 0, 2) == '--' && isset($runmode[substr($arg, 2)]))
	{
		$runmode[substr($arg, 2)] = true;
	}
}

// Help mode. Shows allowed argumentents and quit directly
if ($runmode['help'] == true)
{
	echo 'Usage: ' . $argv[0] . ' [runmode]' . "\n";
	echo 'Available runmodes:' . "\n";
	foreach ($runmode as $runmod => $val)
	{
		echo ' --' . $runmod . "\n";
	}
	die();
}

// Setup
$options = array(
	'appName' => 'video-downloader',
	'appDir' => dirname(__FILE__),
	'appDescription' => 'Download queued videos',
	'authorName' => 'Eduardo Russo',
	'authorEmail' => 'tubelivery@tubelivery.com',
	'sysMaxExecutionTime' => '0',
	'sysMaxInputTime' => '0',
	'sysMemoryLimit' => '384M',
	'appRunAsGID' => 1000,
	'appRunAsUID' => 1000,
);

System_Daemon::setOptions($options);

// With the runmode --write-initd, this program can automatically write a
// system startup file called: 'init.d'
// This will make sure your daemon will be started on reboot
if ($runmode['write-initd'])
{
	System_Daemon::writeAutoRun(TRUE);
}

if (!$runmode['no-daemon'])
{
	// Spawn Daemon
	System_Daemon::start();
}

//error_reporting(E_ALL);

$runningOkay = TRUE;
$loop = TRUE;

$status = INITIAL_STATUS;
$restart_cicle = 0;

System_Daemon::info('Started Daemon');

System_Daemon::setSigHandler(SIGHUP, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));
System_Daemon::setSigHandler(SIGTERM, array('/home/ubuntu/Tubelivery/daemon/video_downloader.php', 'myHandler'));

//System_Daemon::setSigHandler(SIGTERM, 'myHandler');

function myHandler($signal)
{
	System_Daemon::warning($signal);
	if ($signal === SIGTERM)
	{
		System_Daemon::warning('I received the termination signal. ' . $signal);
		// Execute some final code
		// and be sure to:
		System_Daemon::stop();
	}
	elseif ($signal === SIGHUP)
	{
		System_Daemon::warning('I received the start signal. ' . $signal);
		// Execute some final code
		// and be sure to:
		System_Daemon::stop();
	}
}

while (!System_Daemon::isDying() && $runningOkay && $loop)
{
	$videos = get_videos_with_status($status);


	//TODO - search a real priority queue algorithm
	//x% of the videos, according to the priority
	$priority_percent = ceil(((10 - (($status - 1) / 2)) / 10) * count($videos));
	//If this is less than the STATUS_CICLE_BREAK, run for the total STATUS_CICLE_BREAK
	$max_cicle = $priority_percent < STATUS_CICLE_BREAK ? ceil(STATUS_CICLE_BREAK - (5 * $status)) : $priority_percent;

	$max_videos = ceil(count($videos));

	System_Daemon::info("status $status: $max_videos new videos and $max_cicle videos per cicle");

	for ($i = 0; $i < $max_cicle && $i < $max_videos; $i++)
	{
		$start = (float) array_sum(explode(' ', microtime()));
		$num_of_videos = $max_videos > $max_cicle ? $max_cicle : $max_videos;

		System_Daemon::info("status $status: " . ($i + 1) . "/$num_of_videos - started download of " . $videos[$i]['channel'] . " - " . $videos[$i]['video_id'] . ".mp4");

		//Download the video
		//TODO - I could start a BG process here
		//What I tought:
		//1 - Start BG process
		//2 - Wait for X sec
		//3 - Verify if /tmp/$quality/$youtube_video_id.part has been created
		//4 - If not, start a process for another video
		$video_info = download_video($videos[$i]['video_id'], $videos[$i]['quality'], $videos[$i]['channel']);


//		[Mar 11 00:19:31]     info: 1/2 - started upload of theonion - V6U8mRBfCi4 with 7.37 MB
//		[Mar 11 00:19:33]      err: ERROR uploading V6U8mRBfCi4.mp4 to S3 [l:147]
//		[Mar 11 00:19:33]     info: 2/2 - started upload of xdadevelopers - 7ZYXQeqpwrU with 18.29 MB
//		[Mar 11 00:19:38]      err: ERROR uploading 7ZYXQeqpwrU.mp4 to S3 [l:147]
		//TODO - create mecaninsm to delete broken files

		//Check error
		if (!isset($video_info['path']))
		{
			$save_path = "/tmp/" . $videos[$i]['quality'] . "/" . $videos[$i]['video_id'] . ".mp4";
			$temp_file = $save_path . ".part";
			System_Daemon::err("ERROR downloading video " . ($i + 1) . "/$num_of_videos");
			foreach($video_info as $v_info)
				System_Daemon::err($v_info);
			
			if (file_exists($save_path))
				unlink($save_path);
			if (file_exists($temp_file))
				unlink($temp_file);
		}
		else
		{
			$save_path = "/tmp/" . $videos[$i]['quality'] . "/" . $videos[$i]['video_id'] . ".mp4";
			$end = (float) array_sum(explode(' ', microtime()));
			$time = sprintf("%.4f", ($end - $start)) . " sec";
			$bytes = number_format($video_info['size'] / 1048576, 2) . ' MB';

			update_video_state_to_downloaded($videos[$i]['id'], $video_info['path'], $video_info['size']);

			System_Daemon::info("downloaded video " . ($i + 1) . "/$num_of_videos to $save_path  with $bytes in $time");
		}
	}
	$status = $status == FINAL_STATUS ? INITIAL_STATUS : $status + STATUS_INCREMENT;

	if ($restart_cicle == RESTART_CICLE)
	{
		$update_status = update_youtube_dl();
		$restart_cicle = 0;
		System_Daemon::info('update_youtube_dl() returned with status ' . $update_status);

		System_Daemon::iterate(DOWNLOAD_SLEEP_TIME * 6);
		System_Daemon::info('restarting video-downloader');
		restart_service("video-downloader");
	}
	else
	{
		$restart_cicle++;
	}
	System_Daemon::info('sleeping ' . DOWNLOAD_SLEEP_TIME . ' seconds');
	System_Daemon::iterate(DOWNLOAD_SLEEP_TIME);
}

if (!$runningOkay)
{
	System_Daemon::err('error');
}
// Shut down the daemon nicely
// This is ignored if the class is actually running in the foreground
System_Daemon::stop();