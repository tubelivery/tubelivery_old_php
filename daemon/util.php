<?php

include('config/base.php');

/**
 * Get all videos with a status
 * @param Mixed $status String or Array
 * @return array array of videos with path 'id', path 'status',
 * youtube 'video_id', youtube 'quality' id, youtube 'channel' id
 */
function get_videos_with_status($status)
{
	if (is_array($status))
	{
		$temp = "";
		foreach ($status as $key => $value)
		{
			$temp .= "$value";
			$temp .= ($key + 1) == sizeof($status) ? '' : ', ';
		}
		$status = $temp;
	}

	$videos_query =
			<<<VIDEOS
			SELECT
				p.id,
				p.status,
				v.youtube_video_id AS video_id,
				q.youtube_quality_id AS quality,
				c.yt_username AS channel
			FROM
				paths AS p
			JOIN
				videos AS v
			ON
				p.video_id = v.id
			JOIN
				channels AS c
			ON
				v.channel_id = c.id
			JOIN
				qualities AS q
			ON
				p.quality_id = q.id
			WHERE
				p.status IN ($status)
			ORDER BY
				DATE_FORMAT(v.created_at, "%Y-%c-%d"),
				v.pub_date
VIDEOS;

	return FETCH($videos_query, PDO::FETCH_ASSOC, false);
}

/**
 * Download a YouTube video from it's URL using youtube-dl script
 * @param string $video_id The YouTube video id
 * @param int $quality The youtube video quality (http://en.wikipedia.org/wiki/YouTube#Quality_and_codecs)
 * @return int The exec return var (0 for success, 1 for fail)
 * */
function download_video($video_id, $quality, $yt_username, $run_in_background = '')
{
	$script = './lib/youtube-dl';
	$video_url = "http://youtube.com/watch?v=$video_id";
	$max_quality = "--max-quality $quality";

	$path = "/tmp/$quality/$video_id.mp4";
	$db_path = "$yt_username/$quality/$video_id.mp4";
	$options = "--quiet --continue --no-progress --no-overwrites --retries=1 --output='$path'";

	// $no_output = "> /dev/null";

	$exec = "$script $video_url $options $max_quality $run_in_background";
	exec($exec, $output, $return_var);

	if ($return_var == 0)
		return array(
			'size' => filesize($path),
			'path' => $db_path,
		);
	else
	{		
		return $output;
	}
}

/**
 * Update youtube-dl script to it's latest version
 * @return int The exec return var (0 for success, 1 for fail)
 * */
function update_youtube_dl()
{
	$command = './lib/youtube-dl';
	$options = "-U";
	// ./lib/youtube-dl -U
	$exec = "$command $options";
	exec($exec, $output, $return_var);
	return $return_var;
}

/**
 * Update a video path in the DB
 * @param int $path_id The ID of the path in the DB
 * @param string $video_path The The location of the file
 * @param int $file_size The size of the downloaded file
 */
function update_video_state_to_downloaded($path_id, $video_path, $file_size)
{
	$path_data = array(
		"id" => $path_id,
		"path" => $video_path,
		"file_size" => $file_size,
		"status" => 100,
	);
	INSERT("paths", 'id', $path_data);
}

/**
 * Update the state
 * @param int $path_id The ID of the path in the DB
 * @param string $video_path The The location of the file
 * @param int $file_size The size of the downloaded file
 */
function update_video_state_to_uploaded_to_S3($path_id)
{
	$path_data = array(
		"id" => $path_id,
		"status" => 0,
	);
	INSERT("paths", 'id', $path_data);
}

function restart_service($service){
	$command = 'sudo service';
	$do = 'restart';
	$no_output = "> /dev/null";
	$exec = "$command $service $do $no_output";
	exec($exec);
}
