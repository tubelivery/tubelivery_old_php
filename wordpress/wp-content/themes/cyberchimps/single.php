<?php
get_header();
?>
<?
/**** CALLED FUNCTIONS HERE:
 * functions.php -> cyberchimps_get_layout
 * hooks.php -> cyberchimps_default_container_classes
 */
?>
<div id="container">
	<div id="content">
		<?php while (have_posts()) : the_post(); ?>

			<?php get_template_part('content', 'single'); ?>

			<div class="more-content">
				<div class="row-fluid">
					<div class="span6 previous-post">
						<?php previous_post_link(); ?>
					</div>
					<div class="span6 next-post">
						<?php next_post_link(); ?>
					</div>
				</div>
			</div>

			<?php
			// If comments are open or we have at least one comment, load up the comment template
			if (comments_open() || '0' != get_comments_number())
				comments_template('', true);
			?>

		<?php endwhile; // end of the loop. ?>
	</div>
</div>

<?php get_footer(); ?>