</div>
<footer class="footer navbar-fixed-bottom">
	<a href = "https://mixpanel.com/f/partner" class="mixpanel-badge">
		<img src = "//cdn.mxpnl.com/site_media/images/partner/badge_light.png" alt = "Mobile Analytics">
	</a>
	<?php
	do_action('cyberchimps_before_footer_widgets');
	dynamic_sidebar('cyberchimps-footer-widgets');
	do_action('cyberchimps_after_footer_widgets');

	do_action('cyberchimps_footer');
	?>
</footer>
<?php wp_footer(); ?>
</body>
</html>