<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tubelivery_wp');

/** MySQL database username */
define('DB_USER', 'tubelivery');

/** MySQL database password */
define('DB_PASSWORD', '5tr0ng15mYars3n0');

/** MySQL hostname */
define('DB_HOST', 'tubelivery-db.caur8mguk2fw.us-east-1.rds.amazonaws.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i*@~P59dNMCf"x:i|x/HfCnclYC*q?C3e*a%q0VmBHFt|s3IFhRmCmlmIVjILg06');
define('SECURE_AUTH_KEY',  '*HMz42F~T2xS!OCAK1w8b;dYtBv@x)D8d3B6PPVbq+KqrYylp|pGs:f?/bLg9uhY');
define('LOGGED_IN_KEY',    '&pMa2Xf4jQ9;lIy6HF5HjEBW!VxI#|VZVbSJYRyC6&VR!8H%T|3rBBY*ViVKW38*');
define('NONCE_KEY',        'wl`O7#AN/~T`nx~x~3:UA+bVQ6Oyn1z4:f!~S|"h1ZcU?dCP2|OcxGTTbqHrm#Ev');
define('AUTH_SALT',        '4(SvebP)4FwvlX"UUuNXNJmv47a/zN(Ou*+rP9K7Qe"U`PoBwIRlv8ug8UkP2bBi');
define('SECURE_AUTH_SALT', 'E;P~a?P(~kVG;hTv1#bAXs|cgA(VJwRnoRhA35&)f~f|C)1(t|uCJ2Zhiq0gN%#(');
define('LOGGED_IN_SALT',   'MkxytrduEaGpy5)y4C`~tFk&/zFoov&rhXeq5Y3Q*;2ZtU;Ur+4oq4??qNXcK5iE');
define('NONCE_SALT',       'eiFH_M;M_X@bxka7B@*SEob@yjR~*Ssw4kvLBC+Sdpe"y12IKYZC%3+;Sbi^Y0V5');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_ikpq3d_';

/**
 * Limits total Post Revisions saved per Post/Page.
 * Change or comment this line out if you would like to increase or remove the limit.
 */
define('WP_POST_REVISIONS',  10);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

